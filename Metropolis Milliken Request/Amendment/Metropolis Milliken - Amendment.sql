USE SFDC_DBAMP

--Obtaining Project id whose Record Type equals Mover.
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
	DROP TABLE TEMPDB..#TMP

SELECT DISTINCT p.ID
	 , ISNULL(P.MoverIdPublishNew__c, '') AS MOVERID
	 , ISNULL(p.MoverStage__c, '') AS Type
	 , ISNULL(CONVERT(VARCHAR, P.MoverOccupancyDate__c, 106), '') AS MoveDate
	 , ISNULL(P.MoverOccupancyType__c, '') AS MoveDateType
	 , ISNULL(CONVERT(VARCHAR, P.LeaseExpiry__c, 106), '') AS LeaseExpiry
	 , ISNULL(CONVERT(VARCHAR, p.FLOORAREA__C), '') AS FLOORAREA
	 , ISNULL(CONVERT(VARCHAR, p.FloorAreaFeet__C), '') AS FloorAreaFeet
	 , REPLACE(REPLACE(CONVERT(VARCHAR(MAX), p.Description__c), CHAR(13), ''), CHAR(10), '') AS [Story]
	 , p.MetropolisId__c as MetropolisID
	 , p.Metropolisupdate__C AS [Update]
	 , P.PublishDate__c AS PublishDate, p.RepublishDate__c AS RePublishDate

INTO #TMP

FROM GR_PROJECT__C p
	INNER JOIN RECORDTYPE RT ON RT.ID = P.RECORDTYPEID
	
WHERE RT.NAME = 'Mover'
	AND (CONVERT(DATE, p.PublishDate__c) BETWEEN '2018-06-01' AND '2020-01-16'
	OR CONVERT(DATE, p.RepublishDate__c) BETWEEN '2018-06-01' AND '2020-01-16')
	AND (CONVERT(DATE, p.PUBLISHDATE__C) <> '2020-01-05'
	AND CONVERT(DATE, p.REPUBLISHDATE__C) <> '2020-01-05')
	AND ISNULL(p.recalltype__C, '') <> 'Duplicate'
	AND p.PUBLISH__C = 1
--	AND p.MetropolisId__c IS NOT NULL

--Obtaining Site details.
IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
	DROP TABLE TEMPDB..#TMP1

SELECT DISTINCT pp.*
	 , ISNULL(s1.ADDRESS1__C, '') AS ADDRESSLINE1
	 , ISNULL(s1.ADDRESS2__C, '') AS ADDRESSLINE2
	 , ISNULL(s1.TOWNNAME__C, '') [TOWN]
	 , ISNULL(s1.COUNTYNAME__C, '') AS COUNTY
	 , ISNULL(s1.POSTCODE__C, '') AS POSTCODE
	 , ISNULL(t.GOV_REGION, '') AS GOV_REGION

INTO #TMP1

FROM #TMP pp
	INNER JOIN vw_Project_with_site_proposal s ON s.PROJECT__C = pp.id
	INNER JOIN GR_SITEPROJECTLINK__C spl ON spl.PROJECT__C = pp.id
	INNER JOIN GR_SITE__C s1 ON s1.ID=spl.SITE__C
	INNER JOIN vw_Town_County_Region t ON t.TOWN_ID=s1.TOWN__C
	LEFT OUTER JOIN VW_LOCAL_AUTHORITY_COUNTRY LA ON LA.GLOCAL_ID=S.LOCALAUTHORITY__C

WHERE LEN(ISNULL(SPL.SITE__C, '')) > 0

--Obtaining Office details.
IF OBJECT_ID('TEMPDB..#TMP2') IS NOT NULL
	DROP TABLE TEMPDB..#TMP2

SELECT DISTINCT p.*, co.ROLE_EXTERNALID, co.ROLE_NAME AS ROLE_NAME, co.Contract_ID
	 , ISNULL(co.OFFICE_NAME, '') AS Occupier
	 , ISNULL(co.COMPANY_NAME, '') AS COMPANY_NAME
	 , ISNULL(co.OFFICE_ADDRESS1, '') AS ADDR_1
	 , ISNULL(co.OFFICE_ADDRESS2, '') AS ADDR_2
	 , ISNULL(co.OFFICE_SUBDISTRICT, '') AS ADDR_3
	 , ISNULL(c.TOWN_NAME, '') AS TOWN_NAME
	 , ISNULL(c.COUNTY_NAME, '') AS COUNTY_NAME
	 , ISNULL(co.OFFICE_POSTCODE, '') AS POST_CD
	 , ISNULL(co.OFFICE_BUSINESSTYPE, '') AS BUSINESSTYPE
	 , ISNULL(co.OFFICE_EMAIL, '') AS OFFICE_EMAIL
	 , (CASE WHEN co.PHONE1TPS__C = 'Individual TPS' THEN co.PHONE1__C+' (Individual TPS)'
			 WHEN co.PHONE1TPS__C = 'Corporate TPS' THEN co.PHONE1__C+' (Corporate TPS)' ELSE ISNULL(co.PHONE1__C, '') END) PHONE

INTO #TMP2

FROM #TMP1 p
	LEFT OUTER JOIN vw_Contract_Office co ON co.PROJECT__C=p.ID
		AND ISNULL(co.Contract_Deceased, 0) = 0
		AND ISNULL(co.Contract_MPSNoMail, 0) = 0
		AND ISNULL(co.OFFICE_NOLONGERTRADING, 0) = 0
		AND co.OFFICE_PUBLISH = 1
		AND ISNULL(co.OFFICEIDPUBLISHNEW__C, '') <> 1
	LEFT OUTER JOIN vw_Town_County_Region c ON c.TOWN_ID=co.OFFICE_TOWN
WHERE co.Role_Name IN ('Architect', 'Developer', 'Occupier/End User')
	AND c.GOV_REGION = 'W.MID'

--Obtaining Contact details.
IF OBJECT_ID('TEMPDB..#TMP3') IS NOT NULL
	DROP TABLE TEMPDB..#TMP3

SELECT DISTINCT p.*, ISNULL(CC.ContacIdPublishnew__C, '') AS ContactId
	 , ISNULL(CC.Contact_Salutation, '') AS SALUTATION
	 , ISNULL(cc.Contact_Initials, '') AS INITIALS
	 , ISNULL(cc.Contact_FirstName, '') AS FIRST_NAME
	 , ISNULL(cc.Contact_LastName, '') AS LAST_NAME
	 , ISNULL(CC.Contact_Phone, '') AS CONTACT_PHONE
	 , ISNULL(cc.Contact_JobTitle, '') AS JOB_TITLE
	 , ISNULL(cc.Contact_Email, '') AS CONTACT_EMAIL
	 , CC.ContacIdPublishnew__C
INTO #TMP3

FROM #TMP2 p
	LEFT OUTER JOIN vw_Contract_Contact cc ON cc.PROJECT__C=p.ID
		AND cc.Role_Name=p.ROLE_NAME
		AND cc.CONTRACT__C=p.Contract_ID
		AND ISNULL(cc.contact_deceased, 0) = 0
		AND ISNULL(cc.contact_mpsnomail, 0) = 0
		AND cc.CONTACT_PUBLISH = 1

--Aligning the required fields.
IF OBJECT_ID('TEMPDB..#TMP4') IS NOT NULL
	DROP TABLE TEMPDB..#TMP4

SELECT DISTINCT MOVERID [Project ID], MetropolisID [Metropolis ID], Occupier, ROLE_NAME
	 , BUSINESSTYPE [Business Type], MoveDate [Move Date], MoveDateType [Move Date Type], LeaseExpiry [Lease Expiry], [Type], FLOORAREA [Floorspace Sqm], FloorAreaFeet [Floorspace Feet]
	 , ADDRESSLINE1 Address_1, ADDRESSLINE2 Address_2, [TOWN] City, POSTCODE PostCode, COUNTY County, GOV_REGION Region
	 , CASE WHEN CONVERT(DATE, PublishDate) > CONVERT(DATE, ISNULL(RePublishDate, '01-JAN-1900')) THEN ISNULL(CONVERT(VARCHAR, PublishDate, 106), '')
			ELSE ISNULL(CONVERT(VARCHAR, RePublishDate, 106), '') END Dated
	 , [Update], Story, JOB_TITLE Contact_Job_Title, COMPANY_NAME Contact_Company, SALUTATION Contact_Title, FIRST_NAME Contact_Christian_Name, LAST_NAME Contact_Surname
	 , CONTACT_PHONE Contact_Telephone, CONTACT_EMAIL Contact_Email, OFFICE_EMAIL, PHONE
	 , REPLACE(REPLACE(ISNULL(ADDR_1,'')+','+ISNULL(ADDR_2,'')+','+ISNULL(ADDR_3,'')+','+ISNULL(TOWN_NAME,'')+','+ISNULL(COUNTY_NAME,'')+','+POST_CD,',,,',','),',,',',') Contact_Address
	 , REPLACE(REPLACE(ISNULL(ADDRESSLINE1,'')+','+ISNULL(ADDRESSLINE2,'')+','+ISNULL(TOWN,'')+','+ISNULL(COUNTY,'')+','+POSTCODE,',,,',','),',,',',') Project_Address
	 , CONCAT(SALUTATION, ' ', FIRST_NAME, ' ', LAST_NAME) Contact, ContactId

INTO #TMP4

FROM #TMP3 p

--Final Select Statement to fetch the required output.
SELECT DISTINCT CASE WHEN [Metropolis ID] IS NULL THEN [Project ID] ELSE [Metropolis ID] END [Metropolis ID]
	 , ROLE_NAME [Role Name], Occupier AS Company, [Business Type], [Move Date], [Type], [Floorspace Sqm], Address_1, Address_2, City, PostCode, County, Region, Dated, [Update]
	 , Story, Contact_Job_Title, Contact_Company, Contact_Title, Contact_Christian_Name, Contact_Surname, Contact_Telephone, Contact_Email
	 , CASE WHEN CHARINDEX(',', Contact_Address) = 1 THEN SUBSTRING(Contact_Address, 2, LEN(Contact_Address)) ELSE Contact_Address END Contact_Address

FROM #TMP4 a

--Drop Intermediate Tables.

IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
	DROP TABLE TEMPDB..#TMP

IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
	DROP TABLE TEMPDB..#TMP1

IF OBJECT_ID('TEMPDB..#TMP2') IS NOT NULL
	DROP TABLE TEMPDB..#TMP2

IF OBJECT_ID('TEMPDB..#TMP3') IS NOT NULL
	DROP TABLE TEMPDB..#TMP3

IF OBJECT_ID('TEMPDB..#TMP4') IS NOT NULL
	DROP TABLE TEMPDB..#TMP4
