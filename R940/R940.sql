


IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP
SELECT DISTINCT p.ID
	, p.PROJECTIDPUBLISH__C
	, p.MCADATE__C
	, p.VALUE__C
	, co.ROLE_EXTERNALID
	, co.ROLE_NAME
	, ISNULL(co.OFFICE_NAME, '') AS OFFICE_NAME
	, ISNULL(co.OFFICEIDPUBLISHNEW__C, '') AS OFFICE_ID
	, co.ULTIMATECOMPANY_NAME
	, co.ULTIMATECOMPANYID__C
	, co.ULTIMATECOMPANY__C
INTO #tmp
FROM vw_PROJECT p
INNER JOIN vw_Contract_Office co ON co.PROJECT__C=p.ID
WHERE P.PROJECTSIZENAME IN ('LARGE', 'MEGA', 'SMALL')
	AND ISNULL(p.recalltype__C,'') <> 'Duplicate'
	AND ISNULL(p.PROJECT_RECORDTYPE, '') <> 'Mover'
	AND p.PUBLISH__C = 1
	AND p.MCADATE__C BETWEEN '2018-12-01' AND '2019-11-30' 
	AND p.VALUE__C BETWEEN 10 AND 30
	AND ISNULL(co.Contract_Deceased, 0) = 0
	AND ISNULL(co.Contract_MPSNoMail, 0) = 0
	AND ISNULL(co.OFFICE_NOLONGERTRADING, 0) = 0
	AND co.OFFICE_PUBLISH = 1
	AND ISNULL(co.OFFICEIDPUBLISHNEW__C, '') <> 1
	AND co.ROLE_EXTERNALID like '17__'

--1545


IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
DROP TABLE TEMPDB..#TMP1
;with CTE as (
select distinct  a.ULTIMATECOMPANY_NAME, a.ULTIMATECOMPANYID__C
	, o.OfficeType__c
	, o.Name
	, o.OfficeIdPublishNew__c
	, o.Phone1__c
	, o.Phone2__c
	, o.Postcode__c 
	, o.DetailsCheckedDate__c
	, c.CompanyIdPublishNew__c
	, c.BusinessName__c AS [Business Name]
	, c.RegistrationNumber__c AS [Registration Number]
	, c.HoldingCompanyName__c AS [Holding Company Name]
	, c.UltimateHoldingCompanyName__c AS [Ultimate Holding Company Name]
	, CASE WHEN c.LimitedAdded__c = 1 THEN 'Yes' ELSE 'No' END AS [Limited Added]
	, c.LegalForm__c AS [Legal Form]
	, c.Turnover__c AS [Turnover]
	, c.TotalAssets__c AS [Total Current Assets]
	, ISNULL(CONVERT(VARCHAR, c.AccountsDate__c, 106), '') AS [Date of Accounts]
	, ISNULL(CONVERT(VARCHAR, c.IncorporationDate__c, 106), '') AS [Incorporation Date]
	, c.NumberOfEmployees__c AS [Number Of Employees]
	, c.SICCodeCode__c AS [SIC Code]
	, ISNULL(c.Address1__c+ ', ','') + ISNULL(c.Address2__c+ ', ','') + ISNULL(c.Address3__c+ ', ','') + ISNULL(c.Address4__c+ ', ','') Address
	, c.Postcode__c AS PostCode

from #tmp a
LEFT JOIN GR_Office__c o ON o.UltimateCompanyId__c = a.UltimateCompanyId__c COLLATE Latin1_General_CS_AS 
and o.OfficeType__c = 'Head Office' 
AND o.CompanyId__c = a.UltimateCompanyId__c COLLATE Latin1_General_CS_AS 
AND o.publish__c = 1
AND ISNULL(o.NoLongerTrading__c, 0) = 0
AND ISNULL(o.officeidpublishnew__c, '') <> 1
LEFT JOIN GR_Company__c c ON left(c.id,15) = a.UltimateCompanyId__c COLLATE Latin1_General_CS_AS
)
, cte1 as 
(Select *
	, row_number() OVER(Partition by CompanyIdPublishNew__c ORDER BY CompanyIdPublishNew__c,DetailsCheckedDate__c desc,OfficeIdPublishNew__c) r
FROM CTE
)
select *
INTO #TMP1
from cte1
where r = 1
--760



IF OBJECT_ID('TEMPDB..#TMP2') IS NOT NULL
DROP TABLE TEMPDB..#TMP2
;with cte as(
select ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME, SUM(VALUE__C) AS [Total Project Value],COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [Total no of Project]
from (SELECT DISTINCT ULTIMATECOMPANYID__C,ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C FROM #tmp) a
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
)
, cte1 as (
select *
	, rank() OVER(order by [Total Project Value] Desc ) vr
	, rank() OVER(order by [Total no of Project] DESC) pr
from cte)
select *
INTO #TMP2
from cte1
order by pr asc

--SELECT * FROM #TMP2 ORDER BY VR ASC

-- top 50 rank based on project value
;WITH CTE AS(
SELECT a.ULTIMATECOMPANY_NAME,c.[Total Project Value],c.[Total no of Project]
	, b.Name AS [Head office of the Ultimate company]
	, coalesce(b.Phone1__c,b.phone2__c,'') AS [Head office Phone]
	, b.Postcode__c AS [Head office postcode]
	, b.[Business Name]
	, b.[Registration Number]
	, b.[Holding Company Name]
	, b.[Ultimate Holding Company Name]
	, b.[Limited Added]
	, b.[Legal Form]
	, b.Turnover
	, b.[Total Current Assets]
	, b.[Date of Accounts]
	, b.[Incorporation Date]
	, b.[Number Of Employees]
	, b.[SIC Code]
	, b.Address
	, b.PostCode
FROM #TMP2 C
LEFT JOIN #TMP1 b ON C.ULTIMATECOMPANYID__C=b.ULTIMATECOMPANYID__C COLLATE Latin1_General_CS_AS
LEFT JOIN #TMP A ON c.ULTIMATECOMPANYID__C=a.ULTIMATECOMPANYID__C COLLATE Latin1_General_CS_AS 
where vr < 51 
--FROM #tmp a
--LEFT JOIN #TMP1 b ON a.ULTIMATECOMPANYID__C=b.ULTIMATECOMPANYID__C COLLATE Latin1_General_CS_AS
--LEFT JOIN #TMP2 c ON c.ULTIMATECOMPANYID__C=a.ULTIMATECOMPANYID__C COLLATE Latin1_General_CS_AS AND vr < 51 
)
SELECT DISTINCT *
FROM CTE
ORDER BY [Total Project Value] DESC




-- top 50 rank based on Total project 
;WITH CTE AS(
SELECT a.ULTIMATECOMPANY_NAME,c.[Total Project Value],c.[Total no of Project]
	, b.Name AS [Head office of the Ultimate company]
	, coalesce(b.Phone1__c,b.phone2__c,'') AS [Head office Phone]
	, b.Postcode__c AS [Head office postcode]
	, b.[Business Name]
	, b.[Registration Number]
	, b.[Holding Company Name]
	, b.[Ultimate Holding Company Name]
	, b.[Limited Added]
	, b.[Legal Form]
	, b.Turnover
	, b.[Total Current Assets]
	, b.[Date of Accounts]
	, b.[Incorporation Date]
	, b.[Number Of Employees]
	, b.[SIC Code]
	, b.Address
	, b.PostCode
FROM #TMP2 C
LEFT JOIN #TMP1 b ON C.ULTIMATECOMPANYID__C=b.ULTIMATECOMPANYID__C COLLATE Latin1_General_CS_AS
LEFT JOIN #TMP A ON c.ULTIMATECOMPANYID__C=a.ULTIMATECOMPANYID__C COLLATE Latin1_General_CS_AS 
where Pr < 51 
)
SELECT DISTINCT  *
FROM CTE
ORDER BY [Total no of Project] DESC



--- FOR qc

select ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME, SUM(VALUE__C) AS [Total Project Value],COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [Total no of Project]
from (SELECT DISTINCT ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C FROM #tmp) a
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME


select * from #tmp



