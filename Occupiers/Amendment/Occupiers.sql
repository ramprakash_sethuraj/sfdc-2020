USE SFDC_DBAMP

--Select the MoverId, Latest Publish Date and Mover Tage, Record Type is Mover and role should not be Mover Occupiers and role should be project Contact
--106
--tab1
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP

SELECT DISTINCT p.id, p.MoverIdPublishNew__c AS [Mover ID]
	, CONVERT(VARCHAR(MAX), p.LatestPublishDate__c, 106) AS [Last Published Date] 
	, ISNULL(P.MoverStage__c, '') AS [mover stage] 
INTO #TMP
FROM gr_project__C p 
	INNER JOIN RECORDTYPE rt ON p.RecordTypeId = rt.id
WHERE rt.NAME = 'Mover'
	AND ISNULL(P.RECALLTYPE__C, '') <> 'DUPLICATE'
	AND p.publish__c = 1
	AND p.id NOT IN (SELECT DISTINCT PROJECT__C FROM vw_Contract_Office WHERE ROLE_EXTERNALID = '2000')
	AND p.id IN (SELECT DISTINCT PROJECT__C FROM vw_Contract_Office CO WHERE ROLE_EXTERNALID = '2014' AND ISNULL(co.Contract_Deceased, 0) = 0
																			AND ISNULL(co.Contract_MPSNoMail, 0) = 0
																			AND ISNULL(co.OFFICE_NOLONGERTRADING, 0) = 0
																			AND co.OFFICE_PUBLISH = 1
																			AND ISNULL(co.OFFICEIDPUBLISHNEW__C, '') <> 1)  
-- office name that role has project contact
IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
DROP TABLE TEMPDB..#TMP1
SELECT [Mover ID], [Last Published Date], ISNULL(co.OFFICE_NAME, '') AS [Project Contact Office], [mover stage]
INTO #TMP1
FROM #TMP p
	INNER JOIN vw_Contract_Office co ON co.PROJECT__C=p.ID
WHERE ISNULL(co.Contract_Deceased, 0) = 0
	AND ISNULL(co.Contract_MPSNoMail, 0) = 0
	AND ISNULL(co.OFFICE_NOLONGERTRADING, 0) = 0
	AND co.OFFICE_PUBLISH = 1
	AND ISNULL(co.OFFICEIDPUBLISHNEW__C, '') <> 1
	AND ROLE_EXTERNALID = '2014' 

-- Final Required Fields, 110
SELECT DISTINCT [Mover ID], [Last Published Date], [Project Contact Office], [mover stage]
INTO #TMP1_Tab1
FROM #TMP1

--tab2 Record Type is Mover and role should not be Mover Occupiers
--136
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP
SELECT DISTINCT p.id, p.MoverIdPublishNew__c AS [Mover ID]
	, CONVERT(VARCHAR(MAX), p.LatestPublishDate__c, 106) AS [Last Published Date] 
	, ISNULL(P.MoverStage__c, '') AS [mover stage] 
INTO #TMP
FROM gr_project__C p 
	INNER JOIN RECORDTYPE rt ON p.RecordTypeId = rt.id
WHERE rt.NAME = 'Mover'
	AND ISNULL(P.RECALLTYPE__C, '') <> 'DUPLICATE'
	AND p.publish__c = 1
	AND p.id NOT IN (SELECT DISTINCT PROJECT__C FROM vw_Contract_Office WHERE ROLE_EXTERNALID = '2000')

-- Fetching the Office name that role has 'Project Contact', 140
IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
DROP TABLE TEMPDB..#TMP1
SELECT [Mover ID], [Last Published Date], ISNULL(co.OFFICE_NAME, '') AS [Project Contact Office], [mover stage]
INTO #TMP1
FROM #TMP p
	LEFT JOIN vw_Contract_Office co ON co.PROJECT__C=p.ID
	AND ISNULL(co.Contract_Deceased, 0) = 0
	AND ISNULL(co.Contract_MPSNoMail, 0) = 0
	AND ISNULL(co.OFFICE_NOLONGERTRADING, 0) = 0
	AND co.OFFICE_PUBLISH = 1
	AND ISNULL(co.OFFICEIDPUBLISHNEW__C, '') <> 1
	AND ROLE_EXTERNALID = '2014' 

-- Final Required Fields, 140
SELECT DISTINCT [Mover ID], [Last Published Date], [Project Contact Office], [mover stage]
FROM #TMP1




