
USE SFDC_DBAMP

--Obtaining project details whose size/type in Home Improver, Self Build.
--190411
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP
SELECT DISTINCT p.id, p.PROJECTIDPUBLISH__C AS [Project ID]
	 , ISNULL(p.PROJECT_NAME, '') AS Heading, p.PROJECTSIZENAME AS [Project Size]
	 , REPLACE(REPLACE(CONVERT(VARCHAR(MAX), P.project_DESCRIPTION), CHAR(13), ''), CHAR(10), '') AS Description
	 , ISNULL(p.PLANNINGSTAGE__C, '') AS [Planning Stage]
	 , ISNULL(p.PROJECT_DEVELOPMENTTYPE, '') AS DevelopmentType
INTO #TMP
FROM vw_project p 
	INNER JOIN vw_Project_with_site_proposal s ON s.project__c=p.id
WHERE (p.PROJECTSIZENAME IN ('Home Improver')
	OR p.PROJECTTYPE_NAME IN ('Home Improver'))
	AND ISNULL(p.RECALLTYPE__C, '') <> 'Duplicate'
	AND ISNULL(p.PROJECT_RECORDTYPE, '') <> 'Mover'
	AND P.PUBLISH__C = 1
	AND(P.project_DESCRIPTION like '%Sun rooms%'
	    OR P.project_DESCRIPTION like '%Garden homes%'
		OR P.project_DESCRIPTION like '%orangery rooms%'
		OR P.project_DESCRIPTION like '%Conservatory%')


--Obtaining sector details. 190411
IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
DROP TABLE TEMPDB..#TMP1
SELECT DISTINCT p.*
	 , STUFF((SELECT DISTINCT '~' + a.sector__C FROM vw_project_category a WHERE a.CATEGORYRANKNUMBER__C = 1 AND a.project__C=p.id FOR XML PATH, TYPE).value('.[1]', 'VARCHAR(MAX)'), 1, 1, '') AS [PrimarySectors]
	 , STUFF((SELECT DISTINCT '~' + a.sector__C FROM vw_project_category a WHERE a.CATEGORYRANKNUMBER__C <> 1 AND a.project__C=p.id FOR XML PATH, TYPE).value('.[1]', 'VARCHAR(MAX)'), 1, 1, '') AS [SecondarySectors]
INTO #TMP1
FROM #TMP p

--Obtaining Site Details. 13944
IF OBJECT_ID('TEMPDB..#TMP2') IS NOT NULL
DROP TABLE TEMPDB..#TMP2
SELECT DISTINCT p.*
	 , ISNULL(s1.ADDRESS1__C, '') AS Site_Add1
	 , ISNULL(s1.ADDRESS2__C, '') AS Site_Add2
	 , ISNULL(s1.SUBDISTRICT__C, '') AS Site_Add3
	 , ISNULL(s1.DISTRICT__C, '') AS Site_District
	 , ISNULL(t.TOWN_NAME, '') AS Site_Town
	 , ISNULL(t.COUNTY_NAME, '') AS Site_County
	 , ISNULL(s1.Postcode__c, '') AS Site_Postcode
	 , ISNULL(s1.COUNCILOS__C, '') AS Council_os_source
	 , ISNULL(s.PLANNINGREFERENCE__C, '') AS Application_no
	 , ISNULL(s.PLANNINGAPPLICATIONTYPE__C, '') AS Application_Type
	 , ISNULL(CONVERT(varchar, s.APPLICATIONDATE__C, 106), '') AS Application_Date
	 , s.refuseddate__C, s.withdrawndate__C, s.permissiondate__C
	 , CASE WHEN s.PERMISSIONDATE__C IS NULL AND s.Refuseddate__C IS NULL AND s.Withdrawndate__c  IS NOT NULL THEN 'Withdrawn'
			WHEN s.PERMISSIONDATE__C IS NULL AND s.Withdrawndate__c IS NULL AND s.Refuseddate__C IS NOT NULL  THEN 'Refused'
			WHEN s.Withdrawndate__c IS NULL AND s.Refuseddate__C IS NULL AND  s.permissiondate__c IS NOT NULL THEN 'Granted'
	   ELSE '' END Decision_Type
	 , ISNULL(t.GOV_REGION, '') AS Gov_Region
INTO #TMP2
FROM #TMP1 p
	LEFT OUTER JOIN vw_Project_with_site_proposal s ON s.project__c=p.id
	INNER JOIN GR_SITEPROJECTLINK__C spl ON spl.PROJECT__C = p.id
	LEFT OUTER JOIN GR_SITE__C s1 ON s1.ID=spl.SITE__C
	LEFT OUTER JOIN vw_Town_County_Region t ON t.TOWN_ID=s1.TOWN__C
	LEFT OUTER JOIN vw_Local_Authority_Country la ON la.GLOCAL_ID=s.LOCALAUTHORITY__C
WHERE s.ACTIVEPROPOSAL__C = 1
	AND LEN(ISNULL(spl.SITE__C, '')) > 0
	AND t.GOV_NAME IN ('East Midlands', 'East of England', 'London', 'North East', 'North West', 'South East', 'South West', 'West Midlands', 'Yorkshire and the Humber', 'Wales')
	AND  CONVERT(DATE, s.PERMISSIONDATE__C) BETWEEN '2019-01-11' AND '2020-01-12'

--Obtaining contact details. 25687
IF OBJECT_ID('TEMPDB..#TMP3') IS NOT NULL
DROP TABLE TEMPDB..#TMP3
SELECT DISTINCT p.*, o.ROLE_EXTERNALID
	 , ISNULL(o.Contract_Salutation, '') Salutation
	 , ISNULL(o.Contract_Initials, '') Initials
	 , ISNULL(o.Contract_FirstName, '') FirstName
	 , ISNULL(o.Contract_Lastname, '') LastName
	 , ISNULL(o.Contract_CompanyName, '') Agent
	 , ISNULL(o.Contract_Address1, '') CAddress1
	 , ISNULL(o.Contract_Address2, '') CAddress2
	 , ISNULL(o.Contract_SubDistrict, '') CSubDistrict
	 , ISNULL(o.Contract_District, '') CDistrict
	 , ISNULL(t.TOWN_NAME, '') CTOWN_NAME
	 , ISNULL(t.COUNTY_NAME, '') AS CCounty
	 , ISNULL(o.Contract_Postcode, '')CPostcode
	 , CASE WHEN o.Contract_PhoneTPS = 'Individual TPS' THEN o.Contract_Phone+' (Individual TPS)' WHEN o.Contract_PhoneTPS = 'Corporate TPS' THEN o.Contract_Phone+' (Corporate TPS)' ELSE o.Contract_Phone END Phone
	 , ISNULL(o.Contract_Email,'') AS Email
	 , ISNULL(CONVERT(DATE, o.CONTRACT_CREATEDDATE), '') CONTRACT_CREATEDDATE
INTO #TMP3
FROM #TMP2 p
	LEFT OUTER JOIN vw_Contract_Office o ON o.project__c=p.id
		AND	ISNULL(o.contract_deceased, 0) = 0
		AND ISNULL(o.contract_mpsnomail, 0) = 0
		AND o.contract_publish = 1
	LEFT OUTER JOIN vw_Town_County_Region t ON t.TOWN_ID=o.Contract_Town



--Aligning the required fields whose role id includes 100, 300.
--13944
IF OBJECT_ID('TEMPDB..#TMP4') IS NOT NULL
DROP TABLE TEMPDB..#TMP4
SELECT DISTINCT c1.id, c1.[Project ID], c1.HEADING, ISNULL(c1.Description, '') Description, c1.Site_Add1, c1.Site_Add2, c1.Site_Add3, c1.Site_District, c1.Site_Town, c1.Site_County, c1.Site_Postcode
	 , COUNCIL_OS_SOURCE, ISNULL([PrimarySectors], '') AS [PrimarySectors], ISNULL([SecondarySectors], '') AS [SecondarySectors], [Planning Stage], DevelopmentType, [Project Size], APPLICATION_NO
	 , Application_Type, Application_Date, Decision_Type, Gov_Region, REFUSEDDATE__C, WITHDRAWNDATE__C, PERMISSIONDATE__C
	 , ISNULL(c2.Salutation, '') Salutation
	 , ISNULL(c2.FirstName, '') FirstName
	 , ISNULL(c2.LastName, '') LastName
	 , ISNULL(c2.Email, '') Email
	 , ISNULL(c2.CAddress1, '') [Applicant Add1]
	 , ISNULL(c2.CAddress2, '') [App Add2]
	 , ISNULL(c2.CSubDistrict, '') [App Add3]
	 , ISNULL(c2.CDistrict, '') [App District]
	 , ISNULL(c2.CTOWN_NAME, '') [App Town]
	 , ISNULL(c2.CCounty, '') [App County]
	 , ISNULL(c2.CPostcode, '') [App Postcode]
	 , ISNULL(c2.Phone, '') [App Tel]
	 , ISNULL(c3.Agent, '') Agent
	 , ISNULL(c3.CAddress1, '') [Agent Add1]
	 , ISNULL(c3.CAddress2, '') [Agent Add2]
	 , ISNULL(c3.CSubDistrict, '') [Agent Add3]
	 , ISNULL(c3.CDistrict, '') [Agent District]
	 , ISNULL(c3.CTOWN_NAME, '') [Agent Town]
	 , ISNULL(c3.CCounty, '') [Agent County]
	 , ISNULL(c3.CPostcode, '') [Agent Postcode]
	 , ISNULL(c3.Phone, '') [Agent Tel]
	 , ISNULL(c3.Email, '') [Agent Email]
INTO #TMP4
FROM #TMP3 c1
	LEFT OUTER JOIN (SELECT * FROM
						(SELECT id, Salutation,FirstName,LastName,Email,CAddress1,CAddress2,CSubDistrict,CDistrict,CTOWN_NAME,CCounty,CPostcode,Phone
							  , ROW_NUMBER() OVER(PARTITION BY ID ORDER BY ID, CONTRACT_CREATEDDATE) AS rw_id
						 FROM #TMP3
						 WHERE ROLE_EXTERNALID = 100) c
					 WHERE rw_id = 1) c2 ON c2.ID = c1.ID
	LEFT OUTER JOIN (SELECT * FROM
						(SELECT id, Agent,CAddress1,CAddress2,CSubDistrict,CDistrict,CTOWN_NAME,CCounty,CPostcode,Phone,Email
							  , ROW_NUMBER() OVER(PARTITION BY ID ORDER BY ID, CONTRACT_CREATEDDATE) AS rw_id
						 FROM #TMP3
						 WHERE ROLE_EXTERNALID = 300) c
					 WHERE rw_id = 1) c3 ON c3.ID = c1.ID

--Final Select Statement to fetch the required output. QC Data 13944
SELECT DISTINCT [Project ID], Heading, Site_Add1, Site_Add2, Site_Add3, Site_District, Site_Town, Site_County, Site_Postcode, Council_OS_Source, [PrimarySectors], [SecondarySectors], [Planning Stage]
	 , DevelopmentType, [Project Size], Application_No, Application_Type, Application_Date
	 , CASE WHEN Decision_Type = 'Refused' THEN ISNULL(CONVERT(Varchar,REFUSEDDATE__C,106),'')
			WHEN Decision_Type = 'Withdrawn' THEN ISNULL(CONVERT(Varchar,WITHDRAWNDATE__C,106),'')
			WHEN Decision_Type = 'Granted' THEN ISNULL(CONVERT(varchar,PERMISSIONDATE__C,106),'')
	   ELSE '' END Decision_Date, Decision_Type, Salutation, FirstName, LastName, [Applicant Add1], [App Add2], [App Add3], [App District], [App Town], [App County], [App Postcode], [App Tel]
	 , Email AS [App Email], Agent, [Agent Add1], [Agent Add2], [Agent Add3], [Agent District], [Agent Town], [Agent County], [Agent Postcode], [Agent Tel], [Agent Email], Description, PERMISSIONDATE__C
FROM #TMP4




--Count 1, last 12 months count
--118184

SELECT 'Sun rooms' ,COUNT(DISTINCT [Project ID])
FROM #TMP4
WHERE CONVERT(DATE, PERMISSIONDATE__C) BETWEEN '2019-01-11' AND '2020-01-10'
	AND Description like '%Sun rooms%'

UNION

SELECT 'Garden homes', COUNT(DISTINCT [Project ID])
FROM #TMP4
WHERE CONVERT(DATE, PERMISSIONDATE__C) BETWEEN '2019-01-11' AND '2020-01-10'
	AND Description like '%Garden homes%'

UNION

SELECT 'orangery rooms', COUNT(DISTINCT [Project ID])
FROM #TMP4
WHERE CONVERT(DATE, PERMISSIONDATE__C) BETWEEN '2019-01-11' AND '2020-01-10'
	AND Description like '%orangery rooms%'
UNION 
-- 13944
SELECT 'Conservatory', COUNT(DISTINCT [Project ID])
FROM #TMP4
WHERE CONVERT(DATE, PERMISSIONDATE__C) BETWEEN '2019-01-11' AND '2020-01-10'
	AND Description like '%Conservatory%'

--Count 2, last week data
--
SELECT 'Sun rooms' ,COUNT(DISTINCT [Project ID])
FROM #TMP4
WHERE CONVERT(DATE, PERMISSIONDATE__C) BETWEEN '2020-01-06' AND '2020-01-12'
	AND Description like '%Sun rooms%'

UNION

SELECT 'Garden homes', COUNT(DISTINCT [Project ID])
FROM #TMP4
WHERE CONVERT(DATE, PERMISSIONDATE__C) BETWEEN '2020-01-06' AND '2020-01-12'
	AND Description like '%Garden homes%'

UNION

SELECT 'orangery rooms', COUNT(DISTINCT [Project ID])
FROM #TMP4
WHERE CONVERT(DATE, PERMISSIONDATE__C) BETWEEN '2020-01-06' AND '2020-01-12'
	AND Description like '%orangery rooms%'
UNION 
-- 13944
SELECT 'Conservatory', COUNT(DISTINCT [Project ID])
FROM #TMP4
WHERE CONVERT(DATE, PERMISSIONDATE__C) BETWEEN '2020-01-06' AND '2020-01-12'
	AND Description like '%Conservatory%'


IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP

IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
DROP TABLE TEMPDB..#TMP1

IF OBJECT_ID('TEMPDB..#TMP2') IS NOT NULL
DROP TABLE TEMPDB..#TMP2

IF OBJECT_ID('TEMPDB..#TMP3') IS NOT NULL
DROP TABLE TEMPDB..#TMP3

IF OBJECT_ID('TEMPDB..#TMP4') IS NOT NULL
DROP TABLE TEMPDB..#TMP4