USE SFDC_DBAMP


SELECT DISTINCT p.MoverIdPublishNew__c AS [Mover ID]
	, p.id AS [Salesforce ID]
	, ISNULL(CONVERT(VARCHAR, p.MoverOccupancyDate__c, 106), '') AS [Mover Occupancy Date]
	, ISNULL(CONVERT(VARCHAR, p.PublishDate__c, 106), '') AS [Publish Date]
	, ISNULL(CONVERT(VARCHAR, p.RepublishDate__c, 106), '') AS [Republish Date]

FROM gr_project__C p 
	INNER JOIN RECORDTYPE rt ON p.RecordTypeId = rt.id
WHERE rt.NAME = 'Mover'
	AND ISNULL(P.RECALLTYPE__C, '') <> 'DUPLICATE'
	AND p.publish__c = 1
	AND (CONVERT(DATE,p.MoverOccupancyDate__c) > '2019-01-20'
		OR
		CONVERT(DATE,p.PublishDate__c) > '2019-01-20'
		OR
		CONVERT(DATE,p.RepublishDate__c) > '2019-01-20'
		OR 
		ISNULL(p.PublishDate__c,'') = ''
		OR
		ISNULL(p.RepublishDate__c,'') = '') 

