USE SFDC_DBAMP

--Obtaining Project id whose project size in Small, Large, Mega.
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
	DROP TABLE TEMPDB..#TMP

SELECT DISTINCT p.ID
	 , ISNULL(P.MoverIdPublishNew__c, '') AS MOVERID
	 , ISNULL(p.MoverStage__c, '') AS Type
	 , ISNULL(CONVERT(VARCHAR, P.MoverOccupancyDate__c, 106), '') AS MoveDate
	 , ISNULL(P.MoverOccupancyType__c, '') AS MoveDateType
	 , ISNULL(CONVERT(VARCHAR, P.LeaseExpiry__c, 106), '') AS LeaseExpiry
	 , ISNULL(CONVERT(VARCHAR, p.FLOORAREA__C), '') AS FLOORAREA
	 , ISNULL(CONVERT(VARCHAR, p.FloorAreaFeet__C), '') AS FloorAreaFeet
	 , REPLACE(REPLACE(CONVERT(VARCHAR(MAX), p.Description__c), CHAR(13), ''), CHAR(10), '') AS [Story]
	 , p.MetropolisId__c as MetropolisID
	 , p.Metropolisupdate__C AS [Update]
	 , P.PublishDate__c AS PublishDate, p.RepublishDate__c AS RePublishDate

INTO #TMP

FROM GR_PROJECT__C p
	INNER JOIN RECORDTYPE RT ON RT.ID = P.RECORDTYPEID
	
WHERE RT.NAME = 'Mover'
	AND (CONVERT(DATE, p.PublishDate__c) BETWEEN '2019-11-10' AND '2020-01-10'
	OR CONVERT(DATE, p.RepublishDate__c) BETWEEN '2019-11-10' AND '2020-01-10')
	AND (CONVERT(DATE, p.PUBLISHDATE__C) <> '2020-01-05'
	AND CONVERT(DATE, p.REPUBLISHDATE__C) <> '2020-01-05')
	AND ISNULL(p.recalltype__C, '') <> 'Duplicate'
	AND p.PUBLISH__C = 1
	AND p.MetropolisId__c IS NOT NULL

--Obtaining Site details.
IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
	DROP TABLE TEMPDB..#TMP1

SELECT DISTINCT pp.*
	 , ISNULL(s1.ADDRESS1__C, '') AS ADDRESSLINE1
	 , ISNULL(s1.ADDRESS2__C, '') AS ADDRESSLINE2
	 , ISNULL(s1.TOWNNAME__C, '') [TOWN]
	 , ISNULL(s1.COUNTYNAME__C, '') AS COUNTY
	 , ISNULL(s1.POSTCODE__C, '') AS POSTCODE
	 , ISNULL(t.GOV_REGION, '') AS GOV_REGION

INTO #TMP1

FROM #TMP pp
	INNER JOIN vw_Project_with_site_proposal s ON s.PROJECT__C = pp.id
	INNER JOIN GR_SITEPROJECTLINK__C spl ON spl.PROJECT__C = pp.id
	INNER JOIN GR_SITE__C s1 ON s1.ID=spl.SITE__C
	INNER JOIN vw_Town_County_Region t ON t.TOWN_ID=s1.TOWN__C
	LEFT OUTER JOIN VW_LOCAL_AUTHORITY_COUNTRY LA ON LA.GLOCAL_ID=S.LOCALAUTHORITY__C

WHERE LEN(ISNULL(SPL.SITE__C, '')) > 0
	AND t.GOV_REGION IN ('S.EAST', 'S.WEST', 'LONDON', 'EAST')

--Obtaining Office details.
IF OBJECT_ID('TEMPDB..#TMP2') IS NOT NULL
	DROP TABLE TEMPDB..#TMP2

SELECT DISTINCT p.*, co.ROLE_EXTERNALID, co.ROLE_NAME AS ROLE_NAME, co.Contract_ID
	 , ISNULL(co.OFFICE_NAME, '') AS Occupier
	 , ISNULL(co.COMPANY_NAME, '') AS COMPANY_NAME
	 , ISNULL(co.OFFICE_ADDRESS1, '') AS ADDR_1
	 , ISNULL(co.OFFICE_ADDRESS2, '') AS ADDR_2
	 , ISNULL(co.OFFICE_SUBDISTRICT, '') AS ADDR_3
	 , ISNULL(c.TOWN_NAME, '') AS TOWN_NAME
	 , ISNULL(c.COUNTY_NAME, '') AS COUNTY_NAME
	 , ISNULL(co.OFFICE_POSTCODE, '') AS POST_CD
	 , ISNULL(co.OFFICE_BUSINESSTYPE, '') AS BUSINESSTYPE
	 , ISNULL(co.OFFICE_EMAIL, '') AS OFFICE_EMAIL
	 , (CASE WHEN co.PHONE1TPS__C = 'Individual TPS' THEN co.PHONE1__C+' (Individual TPS)'
			 WHEN co.PHONE1TPS__C = 'Corporate TPS' THEN co.PHONE1__C+' (Corporate TPS)' ELSE ISNULL(co.PHONE1__C, '') END) PHONE

INTO #TMP2

FROM #TMP1 p
	LEFT OUTER JOIN vw_Contract_Office co ON co.PROJECT__C=p.ID
		AND ISNULL(co.Contract_Deceased, 0) = 0
		AND ISNULL(co.Contract_MPSNoMail, 0) = 0
		AND ISNULL(co.OFFICE_NOLONGERTRADING, 0) = 0
		AND co.OFFICE_PUBLISH = 1
		AND ISNULL(co.OFFICEIDPUBLISHNEW__C, '') <> 1
	LEFT OUTER JOIN vw_Town_County_Region c ON c.TOWN_ID=co.OFFICE_TOWN

--Obtaining Contact details.
IF OBJECT_ID('TEMPDB..#TMP3') IS NOT NULL
	DROP TABLE TEMPDB..#TMP3

SELECT DISTINCT p.*, ISNULL(CC.ContacIdPublishnew__C, '') AS ContactId
	 , ISNULL(CC.Contact_Salutation, '') AS SALUTATION
	 , ISNULL(cc.Contact_Initials, '') AS INITIALS
	 , ISNULL(cc.Contact_FirstName, '') AS FIRST_NAME
	 , ISNULL(cc.Contact_LastName, '') AS LAST_NAME
	 , ISNULL(CC.Contact_Phone, '') AS CONTACT_PHONE
	 , ISNULL(cc.Contact_JobTitle, '') AS JOB_TITLE
	 , ISNULL(cc.Contact_Email, '') AS CONTACT_EMAIL
	 , CC.ContacIdPublishnew__C
INTO #TMP3

FROM #TMP2 p
	LEFT OUTER JOIN vw_Contract_Contact cc ON cc.PROJECT__C=p.ID
		AND cc.Role_Name=p.ROLE_NAME
		AND cc.CONTRACT__C=p.Contract_ID
		AND ISNULL(cc.contact_deceased, 0) = 0
		AND ISNULL(cc.contact_mpsnomail, 0) = 0
		AND cc.CONTACT_PUBLISH = 1

--Aligning the required fields.
IF OBJECT_ID('TEMPDB..#TMP4') IS NOT NULL
	DROP TABLE TEMPDB..#TMP4
;with CTE AS (
select id,ROLE_NAME,count(DISTINCT ContacIdPublishnew__C) AS cn

from #TMP3

group by id,ROLE_NAME 
)
, cte1 as(
SELECT id,'Any role' as ROLE_NAME ,count(DISTINCT ContacIdPublishnew__C) AS cn1

from #TMP3

group by id
)
select  *

into #TMP4

from cte

union 

select *

from cte1

order by 1,2

IF OBJECT_ID('TEMPDB..#TMP5') IS NOT NULL
	DROP TABLE TEMPDB..#TMP5

select DISTINCT b.cn as Total_contact
	, c.cn as occupier_count
	, a.*

INTO #TMP5

from #TMP3 a
	Left OUTER JOIN #TMP4 b on a.id=b.id and b.ROLE_NAME= 'Any role'
	Left OUTER JOIN #TMP4 c on a.id=c.id and c.ROLE_NAME= 'Occupier/End User'

order by a.id

IF OBJECT_ID('TEMPDB..#TMP6') IS NOT NULL
	DROP TABLE TEMPDB..#TMP6

SELECT DISTINCT MOVERID [Project ID], MetropolisID [Metropolis ID], Occupier, ROLE_NAME
	 , BUSINESSTYPE [Business Type], MoveDate [Move Date], MoveDateType [Move Date Type], LeaseExpiry [Lease Expiry], [Type], FLOORAREA [Floorspace Sqm], FloorAreaFeet [Floorspace Feet]
	 , ADDRESSLINE1 Address_1, ADDRESSLINE2 Address_2, [TOWN] City, POSTCODE PostCode, COUNTY County, GOV_REGION Region
	 , CASE WHEN CONVERT(DATE, PublishDate) > CONVERT(DATE, ISNULL(RePublishDate, '01-JAN-1900')) THEN ISNULL(CONVERT(VARCHAR, PublishDate, 106), '')
			ELSE ISNULL(CONVERT(VARCHAR, RePublishDate, 106), '') END Dated, [Update]
	 , Story-- , SCHEMEDESCRIPTION Story
	 , JOB_TITLE Contact_Job_Title, COMPANY_NAME Contact_Company, SALUTATION Contact_Title, FIRST_NAME Contact_Christian_Name, LAST_NAME Contact_Surname
	 , CONTACT_PHONE Contact_Telephone, CONTACT_EMAIL Contact_Email, OFFICE_EMAIL, PHONE
	 , REPLACE(REPLACE(ISNULL(ADDR_1,'')+','+ISNULL(ADDR_2,'')+','+ISNULL(ADDR_3,'')+','+ISNULL(TOWN_NAME,'')+','+ISNULL(COUNTY_NAME,'')+','+POST_CD,',,,',','),',,',',') Contact_Address
	 , REPLACE(REPLACE(ISNULL(ADDRESSLINE1,'')+','+ISNULL(ADDRESSLINE2,'')+','+ISNULL(TOWN,'')+','+ISNULL(COUNTY,'')+','+POSTCODE,',,,',','),',,',',') Project_Address
	 , CONCAT(SALUTATION, ' ',FIRST_NAME,' ',LAST_NAME)Contact, ContactId
	 , Total_contact
	 , occupier_count
	-- , ROW_NUMBER() OVER (PARTITION BY MOVERID, Occupier ORDER BY ContactId DESC) AS Rw_Id

INTO #TMP6

FROM #TMP5 p

IF OBJECT_ID('TEMPDB..#TMP7') IS NOT NULL
	DROP TABLE TEMPDB..#TMP7

SELECT Address_1, Address_2, [Business Type], City, Contact, ContactId, County, Dated, [Floorspace Feet], [Floorspace Sqm], [Lease Expiry], [Metropolis ID], [Move Date], [Move Date Type], Occupier
	, occupier_count, OFFICE_EMAIL, PHONE, PostCode, [Project ID], Project_Address, Region, ROLE_NAME, Story, Total_contact, [Type], [Update]
	, CASE WHEN LEN(Contact_Telephone) = 0 THEN PHONE ELSE Contact_Telephone END AS Contact_Telephone
	, CASE WHEN LEN(Contact_Email) = 0 THEN OFFICE_EMAIL ELSE Contact_Email END AS Contact_Email, Contact_Address, Contact_Title, Contact_Christian_Name, Contact_Surname, Contact_Job_Title, Contact_Company

INTO #TMP7

FROM #TMP6

WHERE occupier_count <> 0
	AND ROLE_NAME= 'Occupier/End User'

UNION  

SELECT Address_1, Address_2, [Business Type], City, Contact, ContactId, County, Dated, [Floorspace Feet], [Floorspace Sqm], [Lease Expiry], [Metropolis ID], [Move Date], [Move Date Type], Occupier
	, occupier_count, OFFICE_EMAIL, PHONE, PostCode, [Project ID], Project_Address, Region, ROLE_NAME, Story, Total_contact, [Type], [Update]
	, PHONE AS Contact_Telephone , OFFICE_EMAIL as Contact_Email, Contact_Address, Contact_Title, Contact_Christian_Name, Contact_Surname, Contact_Job_Title, Contact_Company

FROM #TMP6 

WHERE occupier_count = 0 AND Total_contact = 0
	AND ROLE_NAME= 'Occupier/End User'

UNION

SELECT a.Address_1, a.Address_2, a.[Business Type], a.City, a.Contact, a.ContactId, a.County, a.Dated, a.[Floorspace Feet], a.[Floorspace Sqm], a.[Lease Expiry], a.[Metropolis ID], a.[Move Date], a.[Move Date Type], a.Occupier
	, a.occupier_count, a.OFFICE_EMAIL, a.PHONE, a.PostCode, a.[Project ID], a.Project_Address, a.Region, a.ROLE_NAME, a.Story, a.Total_contact, a.[Type], a.[Update]
	, CASE WHEN LEN(b.Contact_Telephone) = 0 THEN b.PHONE ELSE b.Contact_Telephone END AS Contact_Telephone
	 , CASE WHEN LEN(b.Contact_Email) = 0 THEN b.OFFICE_EMAIL ELSE b.Contact_Email END AS Contact_Email
	 , b.Contact_Address, b.Contact_Title, b.Contact_Christian_Name, b.Contact_Surname, b.Contact_Job_Title, b.Contact_Company

FROM #TMP6 a
	INNER JOIN #TMP6 b ON a.[Project ID]=b.[Project ID] AND b.ROLE_NAME <> 'Occupier/End User'

WHERE a.occupier_count = 0 AND a.Total_contact <> 0
	AND a.ROLE_NAME = 'Occupier/End User'
	AND a.Occupier = b.Occupier

UNION

SELECT distinct a.Address_1, a.Address_2, a.[Business Type], a.City, a.Contact, a.ContactId, a.County, a.Dated, a.[Floorspace Feet], a.[Floorspace Sqm], a.[Lease Expiry], a.[Metropolis ID], a.[Move Date], a.[Move Date Type], a.Occupier
	, a.occupier_count, a.OFFICE_EMAIL, a.PHONE, a.PostCode, a.[Project ID], a.Project_Address, a.Region, a.ROLE_NAME, a.Story, a.Total_contact, a.[Type], a.[Update]
	, a.PHONE AS Contact_Telephone , a.OFFICE_EMAIL as Contact_Email, a.Contact_Address, a.Contact_Title, a.Contact_Christian_Name, a.Contact_Surname, a.Contact_Job_Title, a.Contact_Company

FROM #TMP6 a
	INNER JOIN #TMP6 b ON a.[Project ID]=b.[Project ID] AND b.ROLE_NAME <> 'Occupier/End User'

WHERE a.occupier_count = 0 AND a.Total_contact <> 0
	AND a.ROLE_NAME = 'Occupier/End User'
	AND a.Occupier <> b.Occupier

UNION

SELECT a.Address_1, a.Address_2, a.[Business Type], a.City, a.Contact, a.ContactId, a.County, a.Dated, a.[Floorspace Feet], a.[Floorspace Sqm], a.[Lease Expiry], a.[Metropolis ID], a.[Move Date], a.[Move Date Type], a.Occupier
	, a.occupier_count, a.OFFICE_EMAIL, a.PHONE, a.PostCode, a.[Project ID], a.Project_Address, a.Region, a.ROLE_NAME, a.Story, a.Total_contact, a.[Type], a.[Update]
	, CASE WHEN LEN(b.Contact_Telephone) = 0 THEN b.PHONE ELSE b.Contact_Telephone END AS Contact_Telephone
	 , CASE WHEN LEN(b.Contact_Email) = 0 THEN b.OFFICE_EMAIL ELSE b.Contact_Email END AS Contact_Email
	 , b.Contact_Address, b.Contact_Title, b.Contact_Christian_Name, b.Contact_Surname, b.Contact_Job_Title, b.Contact_Company

FROM #TMP6 a
	INNER JOIN #TMP6 b ON a.[Project ID]=b.[Project ID] AND b.ROLE_NAME <> 'Occupier/End User'
	LEFT OUTER JOIN (select MOVERID,ROLE_NAME,count(DISTINCT ContacIdPublishnew__C) AS RoleCount
					 from #TMP3
					 group by MOVERID,ROLE_NAME) Rc ON Rc.MOVERID = b.[Project ID] AND b.ROLE_NAME = Rc.ROLE_NAME

WHERE a.occupier_count = 0 AND a.Total_contact <> 0 AND Rc.RoleCount <> 0
	AND a.ROLE_NAME = 'Occupier/End User'
	AND a.Occupier <> b.Occupier

--Final Select Statement to fetch the required output.
SELECT DISTINCT [Metropolis ID], Occupier AS Company, [Business Type], [Move Date], [Type], [Floorspace Sqm], Address_1, Address_2, City, PostCode, County, Region, Dated, [Update]
	 , Story, Contact_Job_Title, Contact_Company, Contact_Title, Contact_Christian_Name, Contact_Surname, Contact_Telephone, Contact_Email
	 , CASE WHEN CHARINDEX(',', Contact_Address) = 1 THEN SUBSTRING(Contact_Address, 2, LEN(Contact_Address)) ELSE Contact_Address END Contact_Address

FROM #TMP7 a

--Drop Intermediate Tables.

IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
	DROP TABLE TEMPDB..#TMP

IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
	DROP TABLE TEMPDB..#TMP1

IF OBJECT_ID('TEMPDB..#TMP2') IS NOT NULL
	DROP TABLE TEMPDB..#TMP2

IF OBJECT_ID('TEMPDB..#TMP3') IS NOT NULL
	DROP TABLE TEMPDB..#TMP3

IF OBJECT_ID('TEMPDB..#TMP7') IS NOT NULL
	DROP TABLE TEMPDB..#TMP7

IF OBJECT_ID('TEMPDB..#TMP4') IS NOT NULL
	DROP TABLE TEMPDB..#TMP4

IF OBJECT_ID('TEMPDB..#TMP5') IS NOT NULL
	DROP TABLE TEMPDB..#TMP5

IF OBJECT_ID('TEMPDB..#TMP6') IS NOT NULL
	DROP TABLE TEMPDB..#TMP6
