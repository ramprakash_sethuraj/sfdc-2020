USE SFDC_DBAMP


IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP
SELECT ISNULL(cc.CONTACIDPUBLISHNEW__C, '') AS CONTACT_ID
	 , ISNULL(CC.Contact_Salutation, '') AS SALUTATION
	 , ISNULL(cc.Contact_FirstName, '') AS [First Name]
	 , ISNULL(cc.Contact_LastName, '') AS Surname
	 , ISNULL(cc.Contact_JobTitle, '') AS JOB_TITLE
	 , ISNULL(co.OFFICE_NAME, '') AS [Office Name]
	 , ISNULL(cc.Contact_Email, '') AS [Email Adddress]
	 , co.ROLE_NAME AS ROLE_NAME
	 , cc.Contact_DetailsCheckedDate
	 , co.CONTRACT_CREATEDDATE
INTO #TMP
FROM vw_Contract_Office co
INNER JOIN vw_Contract_Contact cc ON cc.CONTRACT__C=co.Contract_ID
WHERE 	ISNULL(co.Contract_Deceased, 0) = 0
		AND ISNULL(co.Contract_MPSNoMail, 0) = 0
		AND ISNULL(co.OFFICE_NOLONGERTRADING, 0) = 0
		AND co.OFFICE_PUBLISH = 1
		AND ISNULL(co.OFFICEIDPUBLISHNEW__C, '') <> 1
		AND cc.Role_Name=co.ROLE_NAME
		AND ISNULL(cc.contact_deceased, 0) = 0
		AND ISNULL(cc.contact_mpsnomail, 0) = 0
		AND cc.CONTACT_PUBLISH = 1
		AND ISNULL(cc.Contact_Email, '') <> ''
		AND CONVERT(DATE, cc.Contact_DetailsCheckedDate) >= DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())-365, 0)


IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
DROP TABLE TEMPDB..#TMP1

;WITH CTE AS(
SELECT *
	, ROW_NUMBER() OVER (partition by CONTACT_ID order by CONTACT_ID, CONTRACT_CREATEDDATE DESC) r
FROM #TMP
WHERE ROLE_NAME IN ('Managing Contractor', 'Main Contractor', 'Civil Contractor', 'Design Manage Construct', 'Prime Contractor', 'Fit Out Contractor', 'Modular Contractor', 'Turnkey Contractor', 'Const Managemnt Contract', 'PFI/PPP Consortia'
					, 'Management Fee Contr', 'General Builder', 'Design & Build Contr', 'Management Contractor', 'Mech & Elec Contractor', 'Electrical Contractor', 'Mechanical Contractor', 'Block Paving Contractor', 'Paving Contractor'
					, 'Road Surface Contractor', 'Earthworks Contractor', 'Roadworks Contractor', 'Steelwork Contractor', 'Groundwork Contractor', 'Piling Contractor', 'Demolition Contractor', 'Fitting Out Contractor', 'Other Sub Contractor'
					, 'Acoustic Contractor', 'Aerial/Communication Con', 'Air Conditioning Con', 'Barriers Contractor', 'Blockwork Contractor', 'Brick & Block Contractor', 'Bulking/Hardcore Contr', 'Ceilings Contractor', 'Key Contact'
					, 'Frame Contractor', 'Roofing Contractor', 'Cladding Contractor', 'Windows Contractor', 'Brickwork Contractor', 'Sub Structure Contractor', 'Fire Prevention Contr', 'Glazing Contractor', 'Interior Design Contr'
					, 'Asphalting Contractor', 'Cleaning Contractor', 'Concreting Contractor', 'Conservatory Contractor', 'Curtain Walling Contr', 'Doors Contractor', 'Drainage Contractor', 'Dredging Contractor', 'Drilling/Chasing Contr'
					, 'Pre Cast Units Contr', 'Foundation Contractor', 'Rendering Contractor', 'Shop Fitting Contractor', 'Underpinning Contractor', 'Plant Hire Contractor', 'Plastering Contractor', 'Plumbing Contractor', 'Refrigeration Contractor'
					, 'Refuse Disposal Contr', 'Reinforced Conc Wk Contr', 'Retaining Walls Contr', 'Scaffolding Contractor', 'Screeding Contractor', 'Security Contractor', 'Shopfronts/Signage Contr', 'Site Clearance Contr', 'Site Enablement Contr'
					, 'Siteworks Contractor', 'Sports & Leisure Contr', 'Staircases Contractor', 'Steel Fabrication Contr', 'Steel Frame Contractor', 'Steel Structures Contr', 'Stonework Contractor', 'Storage Tanks Contr', 'Street Lighting Contr'
					, 'Structural Contractor', 'Structural Steelwk Contr', 'Tiling Contractor', 'Traffic Mngmnt Contr', 'Vibro Compaction Contr', 'Walling Contractor', 'Water Proofing Contr', 'Water Supply Contractor', 'Balustrades Contractor'
					, 'Carpentry/Joinery Contr', 'Steel Work Contractor', 'Dry Lining Contractor', 'Ducting Contractor', 'Dynamic Compaction Contr', 'Fencing Contractor', 'Fire Protection Contr', 'Flooring Contractor', 'Formwork Contractor'
					, 'Lifts/Escalators Contr', 'Grouting Contractor', 'Heat/Ventilation Contr', 'Insulation Contractor', 'Kitchens Contractor', 'Landscaping Contractor', 'Lighting Contractor', 'Marble Contractor', 'Mastic Joining Contr'
					, 'Material Handling Contr', 'Metal Work Contractor', 'Paint & Decorate Contr', 'Partitioning Contractor', 'Construction Manager', 'Construction Advisor', 'CDM Co-ordinator', 'Project Manager'
					, 'Architect')
	OR JOB_TITLE like '%Technology%'
	OR JOB_TITLE like '%Innovation%'
	OR JOB_TITLE like '%Project Lead%'
	OR JOB_TITLE like '%Project Owner%'
	OR JOB_TITLE like '%Project Manager%'
	OR JOB_TITLE like '%Project Director%'
	OR JOB_TITLE like '%Owner%'
	OR JOB_TITLE like '%Operations Manager%'
	OR JOB_TITLE like '%Operations Director%'
	OR JOB_TITLE like '%Preconstruction Owner%'
	OR JOB_TITLE like '%Preconstruction Manager%'
	OR JOB_TITLE like '%Preconstruction Director%'
	OR JOB_TITLE like '%Estimator%'
	OR JOB_TITLE like '%Estimating%'
	OR JOB_TITLE like '%Project Engineer%'
	OR JOB_TITLE like '%Senior Field Personnel%'
	OR JOB_TITLE like '%Health and Safety%'
	OR JOB_TITLE like '%Prefabrication%'
	OR JOB_TITLE like '%modular%'
	OR JOB_TITLE like '%MMC%'
	OR JOB_TITLE like '%modern methods construction%'
)SELECT *
INTO #TMP1
FROM CTE
WHERE r = 1

-- qc
-- select * from #TMP1


--Count
SELECT COUNT(DISTINCT CONTACT_ID) AS [No of Contacts]
FROM #TMP1


--Data
SELECT SALUTATION
	, [First Name]
	, Surname
	, JOB_TITLE AS [Job Title]
	, [Office Name]
	, [Email Adddress]
	, ROLE_NAME AS [Role on Project]
FROM #TMP1

