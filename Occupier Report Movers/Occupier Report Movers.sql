USE SFDC_DBAMP

--Select the MoverId, Publish Date and Record Type is Mover, 129
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP
SELECT DISTINCT p.id, p.MoverIdPublishNew__c AS [Mover ID]
	, CONVERT(VARCHAR(MAX), PublishDate__c, 106) AS [Publish Date] 
INTO #TMP
FROM gr_project__C p 
	INNER JOIN RECORDTYPE rt ON p.RecordTypeId = rt.id
WHERE rt.NAME = 'Mover'
	AND ISNULL(P.RECALLTYPE__C, '') <> 'DUPLICATE'
	AND p.publish__c = 1
	AND p.id NOT IN (SELECT DISTINCT PROJECT__C FROM vw_Contract_Office WHERE ROLE_EXTERNALID = '2000')

 


-- Final Required Fields, 129
SELECT [Mover ID], [Publish Date], COUNT(*) AS [No of Projects]
FROM #TMP
GROUP BY [Mover ID], [Publish Date]

