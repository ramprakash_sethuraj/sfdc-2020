USE SFDC_DBAMP

--Select the SF ID, MoverId, Move From Postcode, [Move To Postcode] for the project is publlish and record type is 'Mover' 
--109674
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP

SELECT DISTINCT p.id AS [Salesforce ID]
	, p.MoverIdPublishNew__c AS [Mover ID]
	, ISNULL(MoverFromPostcode__c, '') AS [Move From Postcode]
	, ISNULL(s.PostCode__c, '') AS [Move To Postcode]
	--, s.name 
INTO #TMP
FROM gr_project__C p 
	INNER JOIN RECORDTYPE rt ON p.RecordTypeId = rt.id
	LEFT JOIN GR_SiteProjectLink__c Spl ON Spl.Project__c = p.Id
	LEFT JOIN GR_Site__c s ON S.ID = SPL.Site__c    
WHERE rt.NAME = 'Mover'
	AND ISNULL(P.RECALLTYPE__C, '') <> 'DUPLICATE'
	AND p.publish__c = 1
	AND LEN(ISNULL(SPL.SITE__C, '')) > 0
	AND p.MoverIdPublishNew__c IS NOT NULL 


-- Final Required Fields Are...
SELECT DISTINCT [Salesforce ID]
	, [Mover ID]
	, [Move From Postcode]
	, [Move To Postcode]
FROM #TMP
