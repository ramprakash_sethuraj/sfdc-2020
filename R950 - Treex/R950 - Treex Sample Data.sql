USE SFDC_DBAMP

--Obtaining project id.
IF OBJECT_ID('TEMPDB..#TMP_R950_Proj') IS NOT NULL
DROP TABLE TEMPDB..#TMP_R950_Proj
SELECT DISTINCT p.ID
INTO #TMP_R950_Proj
FROM vw_PROJECT p
	INNER JOIN vw_Project_with_site_proposal s ON s.PROJECT__C = p.ID
	INNER JOIN vw_Project_Category pc ON pc.PROJECT__C = p.ID
WHERE p.PROJECTSIZENAME IN ('Small', 'Large', 'Mega')
	AND pc.SECTOR__C IN ('Private Housing', 'Social Housing')
	AND CONVERT(DATE, s.APPLICATIONDATE__C) > '2014-09-01'
	AND ISNULL(p.recalltype__C,'') <> 'Duplicate'
	AND ISNULL(p.PROJECT_RECORDTYPE, '') <> 'Mover'
	AND p.PUBLISH__C = 1

--Obtaining project details.
IF OBJECT_ID('TEMPDB..#TMP_R950_Projects') IS NOT NULL
DROP TABLE TEMPDB..#TMP_R950_Projects
SELECT DISTINCT pp.*, p.PROJECTIDPUBLISH__C AS PROJECTID
	 , ISNULL(p.PROJECT_NAME, '') AS HEADING
	 , ISNULL(p.Projectname1, '') AS PROJECT_NAME
	 , ISNULL(CONVERT(VARCHAR, p.VALUE__C), '') AS VALUE
	 , ISNULL(p.PLANNINGSTAGE__C, '') AS PLANNINGSTAGE
	 , ISNULL(p.CONTRACTSTAGE__C, '') AS CONTRACTSTAGE
	 , ISNULL(CONVERT(VARCHAR, p.STARTDATE__C, 106), '') AS STARTDATE
	 , ISNULL(CONVERT(VARCHAR, p.ENDDATE__C, 106), '') AS ENDDATE
	 , ISNULL(p.PERIODDESC__C, '') AS CONTRACTPERIOD
	 , ISNULL(p.PROJECT_DEVELOPMENTTYPE, '') AS DEV_TYPE
	 , ISNULL(CONVERT(VARCHAR, p.FLOORAREA__C), '') AS FLOORAREA
	 , ISNULL(CONVERT(VARCHAR, p.UNITS__C), '') AS UNITS
	 , ISNULL(CONVERT(VARCHAR, p.STOREYS__C), '') AS STOREYS
	 , ISNULL(CONVERT(VARCHAR, HEIGHTINMETRES__C), '') AS HEIGHT_IN_METRES
	 , ISNULL(CONVERT(VARCHAR, p.LengthInKm__c), '') AS LENGTH_IN_KM
	 , REPLACE(REPLACE(CONVERT(VARCHAR(MAX), p.PROJECT_DESCRIPTION), CHAR(13), ''), CHAR(10), '')AS SCHEMEDESCRIPTION
	 , ISNULL(p.PROJECTSIZENAME, '') AS PROJECT_SIZE
	 , ISNULL(REPLACE(REPLACE(CONVERT(VARCHAR(MAX), B.BODY), CHAR(13), ''), CHAR(10), ''), '') AS LATESTINFORMATION
	 , REPLACE(REPLACE(B.TITLE, CHAR(13), ''), CHAR(10), '')TITLE
	 , ISNULL(CONVERT(VARCHAR, p.PUBLISHDATE__C, 106), '') AS FIRSTPUBLISHEDDATE
	 , ISNULL(CONVERT(VARCHAR, p.lastmodifieddate, 106), '') AS LASTRESEARCHEDDATE, PUBLISHDATE__C, REPUBLISHDATE__C
	 , ISNULL(p.PROJECTSTATUSNAME__C, '') AS PROJECTSTATUS
INTO #TMP_R950_Projects
FROM #TMP_R950_Proj pp
	INNER JOIN vw_PROJECT p ON p.ID=pp.ID
	LEFT OUTER JOIN (SELECT ParentId, Title, Body, LastModifiedDate, ROW_NUMBER() OVER (PARTITION BY ParentId ORDER BY LastModifiedDate DESC) rw_id
					 FROM Note
					 WHERE Title = 'Latest Notes') b on b.parentid=p.ID
		AND b.rw_id = 1

--Obtaining Sector Details.
IF OBJECT_ID('TEMPDB..#TMP_R950_Sectors') IS NOT NULL
DROP TABLE TEMPDB..#TMP_R950_Sectors
SELECT DISTINCT p.*
	 , ISNULL(pc.SECTOR__C, '') AS PRIMARYSECTORS
	 , ISNULL(pc.CATEGORY_NAME, '') AS PRIMARYCATEGORY
	 , ISNULL(pc.CATEGORYGROUP__C, '') AS SECTOR_GROUP
INTO #TMP_R950_Sectors
FROM #TMP_R950_Projects p
	LEFT OUTER JOIN vw_Project_Category pc ON pc.PROJECT__C=p.ID
		AND pc.CATEGORYRANKNUMBER__C = 1

--Obtaining Material Details.
IF OBJECT_ID('TEMPDB..#TMP_R950_Materials') IS NOT NULL
DROP TABLE TEMPDB..#TMP_R950_Materials
SELECT DISTINCT p.*
	 , STUFF((SELECT DISTINCT ','+a.MATERIAL_NAME FROM vw_Project_Material a WHERE a.project__C=p.id FOR XML PATH, TYPE).value('.[1]', 'VARCHAR(MAX)'), 1, 1, '') AS Materials
INTO #TMP_R950_Materials
FROM #TMP_R950_Sectors p

--Obtaining Site Details.
IF OBJECT_ID('TEMPDB..#TMP_R950_Site') IS NOT NULL
DROP TABLE TEMPDB..#TMP_R950_Site
;WITH CTE AS (
SELECT DISTINCT pp.*
	 , ISNULL(s1.ADDRESS1__C, '') AS ADDRESSLINE1
	 , ISNULL(s1.ADDRESS2__C, '') AS ADDRESSLINE2
	 , ISNULL(s1.SUBDISTRICT__C, '') AS ADDRESSLINE3
	 , ISNULL(s1.TOWNNAME__C, '') [TOWN]
	 , ISNULL(s1.BOROUGHNAME__C, '') AS BOROUGH
	 , ISNULL(s1.COUNTYNAME__C, '') AS COUNTY
	 , ISNULL(s1.POSTCODE__C, '') AS POSTCODE
	 , ISNULL(t.GOV_REGION, '') AS GOV_REGION
	 --, ISNULL(SUBSTRING(S1.COUNCILOSEASTING8__C,2,6), '') AS OS_EASTING
	 --, ISNULL(SUBSTRING(S1.COUNCILOSNORTHING8__C,2,6), '') AS OS_NORTHING
	 --, ISNULL(CONVERT(VARCHAR, S1.SiteGeoLocation__Latitude__s), '') AS LATITUDE
	 --, ISNULL(CONVERT(VARCHAR, S1.SiteGeoLocation__Longitude__s), '') AS LONGITUDE
	 , ISNULL(CONVERT(VARCHAR, s1.AREA__C), '') AS SITE_AREA
	 , ISNULL(s.PLANNINGREFERENCE__C, '') AS LEADPLANNINGAPPLICATIONNUMBER
	 , ISNULL(la.LOCAL_AUTHORITY_NAME, '') AS COUNCILNAME
	 , ISNULL(CONVERT(VARCHAR, s.APPLICATIONDATE__C, 106), '') AS LEAD_APPLICATION_SUBMITTED_DATE
	 , s.refuseddate__C, s.withdrawndate__C, s.permissiondate__C
	 , ISNULL(s.PLANNINGTYPE__C, '') AS PLANNING_TYPE
	 , ISNULL(CONVERT(VARCHAR, s.APPEALDATE__C, 106), '') AS APPEAL_DATE
	 , CASE WHEN s.PERMISSIONDATE__C IS NULL AND s.Refuseddate__C IS NULL AND s.Withdrawndate__c  IS NOT NULL THEN 'Withdrawn'
			WHEN s.PERMISSIONDATE__C IS NULL AND s.Withdrawndate__c IS NULL AND s.Refuseddate__C IS NOT NULL  THEN 'Refused'
			WHEN s.Withdrawndate__c IS NULL AND s.Refuseddate__C IS NULL AND  s.permissiondate__c IS NOT NULL THEN 'Granted'
	   ELSE '' END [DECISION]
	 , ISNULL(s.FULLAPPLICATIONURL__C, '') AS PLANNING_APPLICATION_URL
	 , ISNULL(la.APPLICATIONSURL__C, '') AS LA_URL
	 , ISNULL(s.APPLICATIONIDPUBLISH__C, '') APPLICATIONID
	 , ROW_NUMBER() OVER (PARTITION BY PROJECTID ORDER BY PROJECTID, s.APPLICATIONIDPUBLISH__C DESC) Rw_Id
FROM #TMP_R950_Materials pp
	INNER JOIN vw_Project_with_site_proposal s ON s.PROJECT__C=pp.ID
	INNER JOIN GR_SITEPROJECTLINK__C spl ON spl.PROJECT__C = pp.id
	INNER JOIN GR_SITE__C s1 ON s1.ID=spl.SITE__C
	LEFT OUTER JOIN vw_Local_Authority_Country la ON la.GLOCAL_ID=s.LOCALAUTHORITY__C
	LEFT OUTER JOIN vw_Town_County_Region t ON t.TOWN_ID=s1.TOWN__C
WHERE s.ACTIVEPROPOSAL__C = 1
	AND LEN(ISNULL(spl.SITE__C, '')) > 0
	AND CONVERT(DATE, s.APPLICATIONDATE__C) > '2014-09-01'
	AND S1.CountyName__c = 'Cambridgeshire'
)
SELECT * 
INTO #TMP_R950_Site
FROM CTE 
WHERE Rw_Id = 1

--Obtaining Office details.
IF OBJECT_ID('TEMPDB..#TMP_R950_Offices') IS NOT NULL
DROP TABLE TEMPDB..#TMP_R950_Offices
SELECT DISTINCT p.*, co.ROLE_EXTERNALID, co.ROLE_NAME AS ROLE_NAME, co.Contract_ID
	 , ISNULL(co.OFFICE_NAME, '') AS OFFICE_NAME
	 , ISNULL(co.OFFICEIDPUBLISHNEW__C, '') AS OFFICE_ID
	 , ISNULL(co.OFFICE_ADDRESS1, '') AS ADDR_1
	 , ISNULL(co.OFFICE_ADDRESS2, '') AS ADDR_2
	 , ISNULL(co.OFFICE_SUBDISTRICT, '') AS ADDR_3
	 , ISNULL(c.TOWN_NAME, '') AS TOWN_NAME
	 , ISNULL(c.COUNTY_NAME, '') AS COUNTY_NAME
	 , ISNULL(co.OFFICE_POSTCODE, '') AS POST_CD
	 , ISNULL(c.GOV_REGION, '') AS OFF_GOV_REGION
	 , CASE WHEN co.PHONE1TPS__C = 'Individual TPS' THEN co.PHONE1__C+' (Individual TPS)'
			WHEN co.PHONE1TPS__C = 'Corporate TPS' THEN co.PHONE1__C+' (Corporate TPS)' ELSE ISNULL(co.PHONE1__C, '') END PHONE
	 , ISNULL(co.OFFICE_EMAIL, '') AS OFFICE_EMAIL
	 , ISNULL(co.Contract_Mobile, '') AS MOBILE
	 , co.CONTRACT_CREATEDDATE
INTO #TMP_R950_Offices
FROM #TMP_R950_Site p
	LEFT OUTER JOIN vw_Contract_Office co ON co.PROJECT__C=p.ID
		AND co.Role_Externalid IN ('100', '500')
		AND	ISNULL(co.Contract_Deceased, 0) = 0
		AND ISNULL(co.Contract_MPSNoMail, 0) = 0
		AND ISNULL(co.OFFICE_NOLONGERTRADING, 0) = 0
		AND co.OFFICE_PUBLISH = 1
		AND ISNULL(co.OFFICEIDPUBLISHNEW__C, '') <> 1
		-- AND CONVERT(DATE, co.DETAILSCHECKEDDATE__C) >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)
	LEFT OUTER JOIN vw_Town_County_Region c ON c.TOWN_ID=co.OFFICE_TOWN

--Aligning the required fields.
IF OBJECT_ID('TEMPDB..#TMP_R950_Final') IS NOT NULL
DROP TABLE TEMPDB..#TMP_R950_Final
SELECT DISTINCT PROJECTID, HEADING, PROJECT_NAME, ADDRESSLINE1, ADDRESSLINE2, ADDRESSLINE3, TOWN, BOROUGH, COUNTY, POSTCODE, GOV_REGION, VALUE, PLANNINGSTAGE, CONTRACTSTAGE, STARTDATE, ENDDATE
	 , CONTRACTPERIOD, DEV_TYPE, PROJECT_SIZE, PROJECTSTATUS, SITE_AREA, FLOORAREA, UNITS, STOREYS, PRIMARYSECTORS, PRIMARYCATEGORY, SECTOR_GROUP
	 , ISNULL(MATERIALS, '') MATERIALS, LATESTINFORMATION, SCHEMEDESCRIPTION, LEADPLANNINGAPPLICATIONNUMBER, LEAD_APPLICATION_SUBMITTED_DATE, PLANNING_TYPE
	 , CASE WHEN [DECISION] = 'Refused' THEN ISNULL(convert(Varchar,REFUSEDDATE__C,106),'')
			WHEN [DECISION] = 'Withdrawn' THEN ISNULL(convert(Varchar,WITHDRAWNDATE__C,106),'')
			WHEN [DECISION] = 'Granted' THEN ISNULL(convert(varchar,PERMISSIONDATE__C,106),'')
	   ELSE '' END LEAD_APPLICATION_DECISION_DATE, DECISION, COUNCILNAME, FIRSTPUBLISHEDDATE, LASTRESEARCHEDDATE, PLANNING_APPLICATION_URL, LA_URL
	 , ISNULL(c1.ROLE_NAME, '') AS CLIENT_ROLE_NAME
	 , ISNULL(c1.OFFICE_NAME, '') AS CLIENT_OFFICE_NAME
	 , ISNULL(c1.OFFICE_ID, '') AS CLIENT_OFFICE_ID
	 , ISNULL(c1.ADDR_1, '') AS CLIENT_ADDR_1
	 , ISNULL(c1.ADDR_2, '') AS CLIENT_ADDR_2
	 , ISNULL(c1.ADDR_3, '') AS CLIENT_ADDR_3
	 , ISNULL(c1.TOWN_NAME, '') AS CLIENT_TOWN_NAME
	 , ISNULL(c1.COUNTY_NAME, '') AS CLIENT_COUNTY_NAME
	 , ISNULL(c1.POST_CD, '') AS CLIENT_POST_CD
	 , ISNULL(c1.OFF_GOV_REGION, '') AS CLIENT_GOV_REGION
	 , ISNULL(c1.PHONE, '') AS CLIENT_PHONE
	 , ISNULL(c1.MOBILE, '') AS CLIENT_MOBILE
	 , ISNULL(c1.OFFICE_EMAIL, '') AS CLIENT_OFFICE_EMAIL
	 , ISNULL(c2.ROLE_NAME, '') AS ARCHITECT_ROLE_NAME
	 , ISNULL(c2.OFFICE_NAME, '') AS ARCHITECT_OFFICE_NAME
	 , ISNULL(c2.OFFICE_ID, '') AS ARCHITECT_OFFICE_ID
	 , ISNULL(c2.ADDR_1, '') AS ARCHITECT_ADDR_1
	 , ISNULL(c2.ADDR_2, '') AS ARCHITECT_ADDR_2
	 , ISNULL(c2.ADDR_3, '') AS ARCHITECT_ADDR_3
	 , ISNULL(c2.TOWN_NAME, '') AS ARCHITECT_TOWN_NAME
	 , ISNULL(c2.COUNTY_NAME, '') AS ARCHITECT_COUNTY_NAME
	 , ISNULL(c2.POST_CD, '') AS ARCHITECT_POST_CD
	 , ISNULL(c2.OFF_GOV_REGION, '') AS ARCHITECT_GOV_REGION
	 , ISNULL(c2.PHONE, '') AS ARCHITECT_PHONE
	 , ISNULL(c2.MOBILE, '') AS ARCHITECT_MOBILE
	 , ISNULL(c2.OFFICE_EMAIL, '') AS ARCHITECT_OFFICE_EMAIL
INTO #TMP_R950_Final
FROM #TMP_R950_Offices p
	LEFT OUTER JOIN (SELECT * FROM
						(SELECT id, OFFICE_NAME, OFFICE_ID, ADDR_1, ADDR_2, ADDR_3, TOWN_NAME, COUNTY_NAME, POST_CD, OFF_GOV_REGION, PHONE, MOBILE, OFFICE_EMAIL, ROLE_NAME
							  , ROW_NUMBER() OVER(PARTITION BY ID ORDER BY ID, CONTRACT_CREATEDDATE DESC) AS rw_id
						 FROM #TMP_R950_Offices
						 WHERE ROLE_EXTERNALID = 100) c
					 WHERE rw_id = 1) c1 ON c1.ID = p.ID
	LEFT OUTER JOIN (SELECT * FROM
						(SELECT id, OFFICE_NAME, OFFICE_ID, ADDR_1, ADDR_2, ADDR_3, TOWN_NAME, COUNTY_NAME, POST_CD, OFF_GOV_REGION, PHONE, MOBILE, OFFICE_EMAIL, ROLE_NAME
							  , ROW_NUMBER() OVER(PARTITION BY ID ORDER BY ID, CONTRACT_CREATEDDATE DESC) AS rw_id
						 FROM #TMP_R950_Offices
						 WHERE ROLE_EXTERNALID = 500) c
					 WHERE rw_id = 1) c2 ON c2.ID = p.ID

--Final Select Statement to fetch the required output.
SELECT DISTINCT PROJECTID, HEADING, PROJECT_NAME, ADDRESSLINE1, ADDRESSLINE2, ADDRESSLINE3, TOWN, BOROUGH, COUNTY, POSTCODE, GOV_REGION, VALUE, PLANNINGSTAGE, CONTRACTSTAGE, STARTDATE, ENDDATE
	 , CONTRACTPERIOD, DEV_TYPE, PROJECT_SIZE, PROJECTSTATUS, SITE_AREA, FLOORAREA, UNITS, STOREYS, PRIMARYSECTORS, PRIMARYCATEGORY, SECTOR_GROUP, MATERIALS, LATESTINFORMATION, SCHEMEDESCRIPTION
	 , LEADPLANNINGAPPLICATIONNUMBER, LEAD_APPLICATION_SUBMITTED_DATE, LEAD_APPLICATION_DECISION_DATE, DECISION, COUNCILNAME, FIRSTPUBLISHEDDATE, LASTRESEARCHEDDATE, CLIENT_ROLE_NAME ROLE_NAME
	 , CLIENT_OFFICE_NAME OFFICE_NAME, CLIENT_OFFICE_ID OFFICE_ID, CLIENT_ADDR_1 ADDR_1, CLIENT_ADDR_2 ADDR_2, CLIENT_ADDR_3 ADDR_3, CLIENT_TOWN_NAME TOWN_NAME, CLIENT_COUNTY_NAME COUNTY_NAME
	 , CLIENT_POST_CD POST_CD, CLIENT_GOV_REGION GOV_REGION, CLIENT_PHONE PHONE, CLIENT_MOBILE MOBILE, CLIENT_OFFICE_EMAIL OFFICE_EMAIL, ARCHITECT_ROLE_NAME ROLE_NAME
	 , ARCHITECT_OFFICE_NAME OFFICE_NAME, ARCHITECT_OFFICE_ID OFFICE_ID, ARCHITECT_ADDR_1 ADDR_1, ARCHITECT_ADDR_2 ADDR_2, ARCHITECT_ADDR_3 ADDR_3, ARCHITECT_TOWN_NAME TOWN_NAME
	 , ARCHITECT_COUNTY_NAME COUNTY_NAME, ARCHITECT_POST_CD POST_CD, ARCHITECT_GOV_REGION GOV_REGION, ARCHITECT_PHONE PHONE, ARCHITECT_MOBILE MOBILE, ARCHITECT_OFFICE_EMAIL OFFICE_EMAIL
FROM #TMP_R950_Final
WHERE PROJECTID = '19037093'