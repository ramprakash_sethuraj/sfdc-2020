USE SFDC_DBAMP

-- Project Size is Large, Mega and Small, Live Projects
--662031
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP
SELECT DISTINCT P.ID, PROJECTIDPUBLISH__C
INTO #TMP
FROM VW_PROJECT P
WHERE P.PROJECTSIZENAME IN ('LARGE', 'MEGA', 'SMALL')
	AND ISNULL(P.RECALLTYPE__C, '') <> 'DUPLICATE'
	AND ISNULL(p.PROJECT_RECORDTYPE, '') <> 'Mover'
	AND P.PUBLISH__C = 1 AND Archive__c = 0


--Obtaining Office details.232766
IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
DROP TABLE TEMPDB..#TMP1
SELECT DISTINCT p.*, co.ROLE_EXTERNALID, co.ROLE_NAME AS ROLE_NAME, co.Contract_ID
	 , ISNULL(co.OFFICE_NAME, '') AS OFFICE_NAME
	 , ISNULL(co.OFFICEIDPUBLISHNEW__C, '') AS OFFICE_ID
	 , ISNULL(co.OFFICE_ADDRESS1, '') AS ADDR_1
	 , ISNULL(co.OFFICE_ADDRESS2, '') AS ADDR_2
	 , ISNULL(co.OFFICE_SUBDISTRICT, '') AS ADDR_3
	 , ISNULL(c.TOWN_NAME, '') AS TOWN_NAME
	 , ISNULL(c.COUNTY_NAME, '') AS COUNTY_NAME
	 , ISNULL(co.OFFICE_POSTCODE, '') AS POST_CD
	 , ISNULL(c.GOV_REGION, '') AS GOV_REGION
	 , CASE WHEN co.PHONE1TPS__C = 'Individual TPS' THEN co.PHONE1__C+' (Individual TPS)'
			WHEN co.PHONE1TPS__C = 'Corporate TPS' THEN co.PHONE1__C+' (Corporate TPS)' ELSE ISNULL(co.PHONE1__C, '') END PHONE
	 , ISNULL(co.OFFICE_EMAIL, '') AS OFFICE_EMAIL
	 , ISNULL(CONVERT(VARCHAR, co.DETAILSCHECKEDDATE__C, 106), '') AS Office_Checked_Date
INTO #TMP1
FROM #TMP p
	INNER JOIN vw_Contract_Office co ON co.PROJECT__C=p.ID
		AND ISNULL(co.Contract_Deceased, 0) = 0
		AND ISNULL(co.Contract_MPSNoMail, 0) = 0
		AND ISNULL(co.OFFICE_NOLONGERTRADING, 0) = 0
		AND co.OFFICE_PUBLISH = 1
		AND ISNULL(co.officeidpublishnew__C, '') <> 1
		AND co.ROLE_EXTERNALID = '500'
	LEFT OUTER JOIN vw_Town_County_Region c ON c.TOWN_ID=co.OFFICE_TOWN
	    

--Obtaining Contact details. fetching the contact that has office role is arc
--18436920
IF OBJECT_ID('TEMPDB..#TMP_OFF') IS NOT NULL
DROP TABLE TEMPDB..#TMP_OFF
select PROJECTIDPUBLISH__C as Project_Id, o.Officeidpublishnew__c [Office Id], ContacIdPublishNew__c, o.OfficeName__c, ISNULL(c.FirstName__c, '') AS [first name]
	, ISNULL(c.LastName__c, '') surname--, ISNULL(c.Email__c, '') AS [email address]
	, ISNULL(c.JobTitle__c, '') AS [job title] 
	, ISNULL(c.Phone__c, '') AS [phone number] 
	, (  
  CASE   
   WHEN isnull(EO.OPTOUTSTATUS__C, '') NOT IN ('Unsubscribe', 'Only Glenigan Emails Allowed', 'Bounce back')  
    THEN ISNULL(c.EMAIL__C, '')  
   ELSE ''  
   END  
  ) AS [email address]
INTO #TMP_OFF
FROM #TMP1 O1
	JOIN GR_OFFICE__C o on O1.OFFICE_ID = o.OfficeIdPublishNew__c
	JOIN GR_ContactOfficeDates__c  G_COD on G_COD.office__c=o.id
	JOIN RECORDTYPE RT on RT.id=G_COD.RecordTypeId
	JOIN GR_CONTACT__C c on c.Id=G_COD.contact__c
	LEFT JOIN GR_EMailOptOut__c EO ON c.Email__c = EO.Email__c  	
WHERE o.PUBLISH__C = 1
	AND ISNULL(o.NoLongerTrading__c, 0) = 0
	AND ISNULL(o.OFFICEIDPUBLISHNEW__C, '') <> 1
	AND c.PUBLISH__C = 1
	AND ISNULL(c.Deceased__c, 0) = 0
	AND ISNULL(c.MPSNoMail__c, 0) = 0
	AND (Left__c = 0 AND (ToDate__c IS NULL OR CONVERT(DATE,ToDate__c) > CONVERT(DATE,GETDATE())))
	AND CONVERT(DATE, c.DetailsCheckedDate__c) >= DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())-365, 0)

--QC Data, 64415
SELECT DISTINCT [Office Id], ContacIdPublishNew__c as Contact_Id,[first name], surname, [email address] ,[job title] ,[phone number]
INTO TMP_OFFQCData
FROM #TMP_OFF

--select distinct * from TMP_OFFQCData
--drop table TMP_OFFQCData

--Final Output, 
SELECT DISTINCT ContacIdPublishNew__c Contact_Id, [first name], surname, [email address] ,[job title] ,[phone number]
FROM #TMP_OFF
--where [phone number]= '01661 822202'

--Count
--51416
SELECT COUNT(DISTINCT ContacIdPublishNew__c) No_of_Contact
FROM #TMP_OFF





