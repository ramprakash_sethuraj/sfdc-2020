
alter table R961_RIBADataMatch add  id int identity(1,1)
--------- popluate Request Matching with name and Phone number 17

SELECT DISTINCT T1.Id,Phone_Number, CONTACIDPUBLISHNEW__C,  LastName__c
INTO #R961_RIBADataMatch1
FROM R961_RIBADataMatch T1
 INNER JOIN gr_contact__C C ON  c.FirstName__c like '%'+Forenames+'%' and Surname= c.LastName__c
								AND REPLACE(Phone_Number, ' ', '')  = REPLACE(Phone__c, ' ', '') 
WHERE t1.Main_eMail_Address ='' AND t1.Phone_Number !='' 
ORDER BY CONTACIDPUBLISHNEW__C

--SELECT LastName__c+' '+Phone_Number, CONTACIDPUBLISHNEW__C
--FROM #R961_RIBADataMatch1


--------- popluate Request Matching with Email, 1090
SELECT DISTINCT T1.Id, Email__c, ContacIdPublishNew__c
INTO #R961_RIBADataMatch2
FROM gr_contact__C c
INNER JOIN R961_RIBADataMatch t1 ON t1.Main_eMail_Address = C.Email__c
--WHERE Email__c IN (SELECT DISTINCT Main_eMail_Address FROM R961_RIBADataMatch WHERE Main_eMail_Address <>'')


IF OBJECT_ID('TEMPDB..#TMP_OFF') IS NOT NULL
DROP TABLE TEMPDB..#TMP_OFF
select o.Officeidpublishnew__c [Office Id], ContacIdPublishNew__c, o.OfficeName__c, ISNULL(c.FirstName__c, '') AS [first name]
	, ISNULL(c.LastName__c, '') surname--, ISNULL(c.Email__c, '') AS [email address]
	, ISNULL(c.JobTitle__c, '') AS [job title] 
	, ISNULL(c.Phone__c, '') AS [phone number] 
	, (  
  CASE   
   WHEN isnull(EO.OPTOUTSTATUS__C, '') NOT IN ('Unsubscribe', 'Only Glenigan Emails Allowed', 'Bounce back')  
    THEN ISNULL(c.EMAIL__C, '')  
   ELSE ''  
   END  
  ) AS [email address]
  , (  
  CASE   
   WHEN isnull(EO1.OPTOUTSTATUS__C, '') NOT IN ('Unsubscribe', 'Only Glenigan Emails Allowed', 'Bounce back')  
    THEN ISNULL(O.EMAIL__C, '')  
   ELSE ''  
   END  
  ) AS [oFF email address]

INTO #TMP_OFF
FROM GR_OFFICE__C o 
	JOIN GR_ContactOfficeDates__c  G_COD on G_COD.office__c=o.id
	JOIN RECORDTYPE RT on RT.id=G_COD.RecordTypeId
	JOIN GR_CONTACT__C c on c.Id=G_COD.contact__c
	LEFT JOIN GR_EMailOptOut__c EO ON c.Email__c = EO.Email__c 
	LEFT JOIN GR_EMailOptOut__c EO1 ON O.Email__c = EO1.Email__c 	
WHERE o.PUBLISH__C = 1
	AND ISNULL(o.NoLongerTrading__c, 0) = 0
	AND ISNULL(o.OFFICEIDPUBLISHNEW__C, '') <> 1
	AND c.PUBLISH__C = 1
	AND ISNULL(c.Deceased__c, 0) = 0
	AND ISNULL(c.MPSNoMail__c, 0) = 0
	AND (Left__c = 0 AND (ToDate__c IS NULL OR CONVERT(DATE,ToDate__c) > CONVERT(DATE,GETDATE())))
	AND CONVERT(DATE, c.DetailsCheckedDate__c) >= DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())-365, 0)


SELECT DISTINCT T1.Id, t1.Main_eMail_Address , C.[oFF email address], ContacIdPublishNew__c
INTO #R961_RIBADataMatch3
FROM #TMP_OFF c
INNER JOIN R961_RIBADataMatch t1 ON t1.Main_eMail_Address = C.[oFF email address]
AND c.[first name] like '%'+Forenames+'%' and T1.Surname= c.surname
								AND REPLACE(Phone_Number, ' ', '')  = REPLACE([phone number], ' ', '') 
WHERE T1.ID NOT IN (SELECT id FROM #R961_RIBADataMatch1 UNION SELECT ID FROM #R961_RIBADataMatch2)
AND t1.Main_eMail_Address <> ''



SELECT DISTINCT T1.Id, t1.Main_eMail_Address , C.[oFF email address], ContacIdPublishNew__c
INTO #R961_RIBADataMatch4
FROM #TMP_OFF c
INNER JOIN R961_RIBADataMatch t1 ON t1.Main_eMail_Address = C.[oFF email address]
AND c.[first name] like '%'+Forenames+'%' and T1.Surname= c.surname
								--AND REPLACE(Phone_Number, ' ', '')  = REPLACE([phone number], ' ', '') 
WHERE T1.ID NOT IN (SELECT id FROM #R961_RIBADataMatch1 UNION SELECT ID FROM #R961_RIBADataMatch2 UNION SELECT ID FROM #R961_RIBADataMatch3)
AND t1.Main_eMail_Address <> ''


SELECT ID, COUNT(*) FROM #R961_RIBADataMatch3 GROUP BY ID HAVING COUNT(*) > 1


SELECT * FROM #R961_RIBADataMatch4 WHERE ID = 3161
SELECT * FROM R961_RIBADataMatch WHERE ID = 3161

SELECT * FROM #R961_RIBADataMatch3 WHERE ID = 9083



---match 1 first
SELECT ID,ContacIdPublishNew__c  FROM #R961_RIBADataMatch1

---match 2
SELECT * FROM #R961_RIBADataMatch2


-- match 3 final 
; with Cte as
(
select M3.*, ROW_NUMBER() OVER(PARTITION BY Main_eMail_Address ORDER BY Main_eMail_Address,  CreatedDate desc)  AS Row_Id 
--into #R961_RIBADataMatch3Final 
from #R961_RIBADataMatch3 m3
INNER JOIN GR_Contact__c gco ON M3.ContacIdPublishNew__c = gco.ContacIdPublishNew__c
)
SELECT * 
FROM Cte 
WHERE Row_Id = 1

--

-- match 4 final 
; with Cte as
(
select M4.*, ROW_NUMBER() OVER(PARTITION BY Main_eMail_Address ORDER BY Main_eMail_Address,  CreatedDate desc)  AS Row_Id
--into #R961_RIBADataMatch3Final 
from #R961_RIBADataMatch4 m4
INNER JOIN GR_Contact__c gco ON M4.ContacIdPublishNew__c = gco.ContacIdPublishNew__c
--where ContacIdPublishNew__c = '25022568'
)
SELECT * 
FROM Cte 
WHERE Row_Id = 1

--SELECT * FROM R961_RIBADataMatch WHERE ID = 9083



SELECT ID,ContacIdPublishNew__c  FROM #R961_RIBADataMatch1
into 
UNION 

SELECT ID,ContacIdPublishNew__c  FROM #R961_RIBADataMatch2

UNION 

SELECT ID,ContacIdPublishNew__c  FROM #R961_RIBADataMatch3

UNION 

SELECT ID,ContacIdPublishNew__c  FROM #R961_RIBADataMatch4
