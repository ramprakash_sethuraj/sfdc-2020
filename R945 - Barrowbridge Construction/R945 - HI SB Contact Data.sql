USE SFDC_DBAMP

--Obtaining project details whose size/type in Home Improver, Self Build.
IF OBJECT_ID('TEMPDB..#TMP_R945_Projects') IS NOT NULL
DROP TABLE TEMPDB..#TMP_R945_Projects
SELECT DISTINCT p.id, p.PROJECTIDPUBLISH__C AS [Project ID]
	 , REPLACE(REPLACE(CONVERT(VARCHAR(MAX), P.project_DESCRIPTION), CHAR(13), ''), CHAR(10), '') AS Description
INTO #TMP_R945_Projects
FROM vw_project p 
WHERE p.PROJECTIDPUBLISH__C IN ('19423754', '19423812', '19424757', '19424761', '19424817', '19424822', '19424833', '19424846', '19424861', '19424866', '19424891', '19424910', '19424991',
								'19425021', '19425914', '19425950', '19425985', '19425997', '19426022', '19426037', '19426061', '19426084', '19426114', '19426137', '19426151', '19426163',
								'19426193', '19426245', '19426263', '19426300', '19426309', '19426315', '19426352', '19426353', '19426390', '19426399', '19426406', '19426426', '19426465',
								'19426482', '19426496', '19426510', '19426519', '19426523', '19426539', '19426542', '19426577', '19426597', '19426632', '19426637', '19426671', '19426754',
								'19426795', '19426800', '19426801', '19426802', '19426838', '19426846', '19426847', '19427204', '19427066', '19427075', '19427076', '19427079', '19427090',
								'19427133', '19427134', '19427160', '19427189', '19427197', '19427211', '19427221', '19427222', '19427247', '19427256', '19427273', '19427278', '19427284',
								'19427296', '19427298', '19427299', '19427300', '19427921', '19427936', '19427952', '19427971', '19427979', '19427980', '19427982', '19428004', '19428006',
								'19428014', '19428068', '19428030', '19428043', '19428044', '19428047', '19428100', '19428146', '19428183', '19428204', '19428207', '19428275', '19428289',
								'19428295', '19428296', '19428299', '19428315', '19428380', '19428383', '19428455', '19428464', '19428474', '19428477', '19428506', '19428565', '19428566',
								'19428805', '19428622', '19428625', '19428626', '19428659', '19428660', '19428693', '19428694', '19428695', '19428708', '19428711', '19428716', '19428778',
								'19428779', '19428780', '19428799', '19428800', '19428801', '19428802', '19428806', '19428807')

--Obtaining contact details.
IF OBJECT_ID('TEMPDB..#TMP_R945_Contract') IS NOT NULL
DROP TABLE TEMPDB..#TMP_R945_Contract
SELECT DISTINCT p.*, o.ROLE_EXTERNALID
	 , ISNULL(o.Contract_Salutation, '') Salutation
	 , ISNULL(o.Contract_Initials, '') Initials
	 , ISNULL(o.Contract_FirstName, '') FirstName
	 , ISNULL(o.Contract_Lastname, '') LastName
	 , ISNULL(o.Contract_CompanyName, '') Agent
	 , ISNULL(o.Contract_Address1, '') CAddress1
	 , ISNULL(o.Contract_Address2, '') CAddress2
	 , ISNULL(o.Contract_SubDistrict, '') CSubDistrict
	 , ISNULL(o.Contract_District, '') CDistrict
	 , ISNULL(t.TOWN_NAME, '') CTOWN_NAME
	 , ISNULL(t.COUNTY_NAME, '') AS CCounty
	 , ISNULL(o.Contract_Postcode, '')CPostcode
	 , CASE WHEN o.Contract_PhoneTPS = 'Individual TPS' THEN o.Contract_Phone+' (Individual TPS)'
			WHEN o.Contract_PhoneTPS = 'Corporate TPS' THEN o.Contract_Phone+' (Corporate TPS)' ELSE o.Contract_Phone END Phone
	 , ISNULL(o.Contract_Email,'') AS Email
	 , ISNULL(CONVERT(DATE, o.CONTRACT_CREATEDDATE), '') CONTRACT_CREATEDDATE
INTO #TMP_R945_Contract
FROM #TMP_R945_Projects p
	LEFT OUTER JOIN vw_Contract_Office o ON o.project__c=p.id
		AND	ISNULL(o.contract_deceased, 0) = 0
		AND ISNULL(o.contract_mpsnomail, 0) = 0
		AND o.contract_publish = 1
	LEFT OUTER JOIN vw_Town_County_Region t ON t.TOWN_ID=o.Contract_Town

--Aligning the required fields whose role id includes 100, 300.
IF OBJECT_ID('TEMPDB..#TMP_R945_Final') IS NOT NULL
DROP TABLE TEMPDB..#TMP_R945_Final
SELECT DISTINCT c1.id, c1.[Project ID], Description
	 , ISNULL(c2.Salutation, '') Salutation
	 , ISNULL(c2.FirstName, '') FirstName
	 , ISNULL(c2.LastName, '') LastName
	 , ISNULL(c2.CAddress1, '') [Applicant Add1]
	 , ISNULL(c2.CAddress2, '') [App Add2]
	 , ISNULL(c2.CSubDistrict, '') [App Add3]
	 , ISNULL(c2.CDistrict, '') [App District]
	 , ISNULL(c2.CTOWN_NAME, '') [App Town]
	 , ISNULL(c2.CCounty, '') [App County]
	 , ISNULL(c2.CPostcode, '') [App Postcode]
	 , ISNULL(c3.Agent, '') Agent
	 , ISNULL(c3.CAddress1, '') [Agent Add1]
	 , ISNULL(c3.CAddress2, '') [Agent Add2]
	 , ISNULL(c3.CSubDistrict, '') [Agent Add3]
	 , ISNULL(c3.CDistrict, '') [Agent District]
	 , ISNULL(c3.CTOWN_NAME, '') [Agent Town]
	 , ISNULL(c3.CCounty, '') [Agent County]
	 , ISNULL(c3.CPostcode, '') [Agent Postcode]
INTO #TMP_R945_Final
FROM #TMP_R945_Contract c1
	LEFT OUTER JOIN (SELECT * FROM
						(SELECT id, Salutation,FirstName,LastName,CAddress1,CAddress2,CSubDistrict,CDistrict,CTOWN_NAME,CCounty,CPostcode
							  , ROW_NUMBER() OVER(PARTITION BY ID ORDER BY ID, CONTRACT_CREATEDDATE) AS rw_id
						 FROM #TMP_R945_Contract
						 WHERE ROLE_EXTERNALID = 100) c
					 WHERE rw_id = 1) c2 ON c2.ID = c1.ID
	LEFT OUTER JOIN (SELECT * FROM
						(SELECT id, Agent,CAddress1,CAddress2,CSubDistrict,CDistrict,CTOWN_NAME,CCounty,CPostcode
							  , ROW_NUMBER() OVER(PARTITION BY ID ORDER BY ID, CONTRACT_CREATEDDATE) AS rw_id
						 FROM #TMP_R945_Contract
						 WHERE ROLE_EXTERNALID = 300) c
					 WHERE rw_id = 1) c3 ON c3.ID = c1.ID

--Final Select Statement to fetch the required output.
SELECT DISTINCT [Project ID], Description, Salutation, FirstName, LastName, [Applicant Add1], [App Add2], [App Add3], [App District], [App Town], [App County], [App Postcode], Agent, [Agent Add1], [Agent Add2]
	 , [Agent Add3], [Agent District], [Agent Town], [Agent County], [Agent Postcode]
FROM #TMP_R945_Final

