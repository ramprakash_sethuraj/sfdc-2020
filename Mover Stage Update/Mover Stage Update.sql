USE SFDC_DBAMP


SELECT DISTINCT p.MoverIdPublishNew__c AS [Mover ID]
	, p.id AS [Salesforce ID]
	, p.MoverStage__c
	, p.MoverStatus__c 

FROM gr_project__C p 
	INNER JOIN RECORDTYPE rt ON p.RecordTypeId = rt.id
WHERE rt.NAME = 'Mover'
	AND ISNULL(P.RECALLTYPE__C, '') <> 'DUPLICATE'
	AND p.publish__c = 1
	AND ISNULL(p.MoverStage__c,'') = ''
 



