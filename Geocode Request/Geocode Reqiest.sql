USE SFDC_DBAMP

--Obtaining project id.
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP
SELECT DISTINCT P.ID
INTO #TMP
FROM VW_PROJECT P
	INNER JOIN vw_Project_with_site_proposal s ON s.PROJECT__C = p.ID
WHERE P.PROJECTSIZENAME IN ('LARGE', 'MEGA', 'SMALL')
	AND CONVERT(DATE, s.APPLICATIONDATE__C) BETWEEN '2008-01-01' AND '2016-02-28'
	AND ISNULL(P.RECALLTYPE__C, '') <> 'DUPLICATE'
	AND ISNULL(p.PROJECT_RECORDTYPE, '') <> 'Mover'

--Obtaining project details.
IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
DROP TABLE TEMPDB..#TMP1
SELECT DISTINCT PP.*
	 , P.PROJECTIDPUBLISH__C AS PROJECTID
	 , ISNULL(P.PROJECT_NAME, '') AS HEADING
	 , ISNULL(P.PROJECTNAME1, '') AS PROJECT_NAME
	 , ISNULL(CONVERT(VARCHAR, P.VALUE__C), '') AS VALUE
	 , ISNULL(P.PLANNINGSTAGE__C, '') AS PLANNINGSTAGE
	 , ISNULL(P.CONTRACTSTAGE__C, '') AS CONTRACTSTAGE
	 , ISNULL(CONVERT(VARCHAR, P.STARTDATE__C, 106), '') AS STARTDATE
	 , ISNULL(CONVERT(VARCHAR, P.ENDDATE__C, 106), '') AS ENDDATE
	 , ISNULL(P.PERIODDESC__C, '') AS CONTRACTPERIOD
	 , ISNULL(P.PROJECT_DEVELOPMENTTYPE, '') AS DEV_TYPE
	 , ISNULL(CONVERT(VARCHAR, P.FLOORAREA__C), '') AS FLOORAREA
	 , ISNULL(CONVERT(VARCHAR, P.UNITS__C), '') AS UNITS
	 , ISNULL(CONVERT(VARCHAR, P.STOREYS__C), '') AS STOREYS
	 , ISNULL(CONVERT(VARCHAR, HEIGHTINMETRES__C), '') AS HEIGHT_IN_METRES
	 , ISNULL(CONVERT(VARCHAR, P.LENGTHINKM__C), '') AS LENGTH_IN_KM
	 , REPLACE(REPLACE(CONVERT(VARCHAR(MAX), P.PROJECT_DESCRIPTION), CHAR(13), ''), CHAR(10), '') AS SCHEMEDESCRIPTION
	 , ISNULL(P.PROJECTSIZENAME, '') AS PROJECT_SIZE
	 , ISNULL(REPLACE(REPLACE(CONVERT(VARCHAR(MAX), B.BODY), CHAR(13), ''), CHAR(10), ''), '') AS LATESTINFORMATION
	 , REPLACE(REPLACE(B.TITLE, CHAR(13), ''), CHAR(10), '')TITLE
	 , ISNULL(CONVERT(VARCHAR, P.PUBLISHDATE__C, 106), '') AS FIRSTPUBLISHEDDATE
	 , ISNULL(CONVERT(VARCHAR, P.LASTMODIFIEDDATE, 106), '') AS LASTRESEARCHEDDATE
	 , PUBLISHDATE__C, REPUBLISHDATE__C
	 , ISNULL(P.PROJECTSTATUSNAME__C, '') AS PROJECTSTATUS
INTO #TMP1
FROM #TMP PP
	INNER JOIN VW_PROJECT P ON P.ID=PP.ID
	LEFT OUTER JOIN (SELECT ParentId, Title, Body, LastModifiedDate, ROW_NUMBER() OVER (PARTITION BY ParentId ORDER BY LastModifiedDate DESC) rw_id
					 FROM Note
					 WHERE Title = 'Latest Notes') b on b.parentid=p.ID
		AND b.rw_id = 1

--Obtaining Sector Details.
IF OBJECT_ID('TEMPDB..#TMP2') IS NOT NULL
DROP TABLE TEMPDB..#TMP2
SELECT DISTINCT P.*
	 , ISNULL(PC.SECTOR__C, '') AS PRIMARYSECTORS
	 , ISNULL(PC.CATEGORY_NAME, '') AS PRIMARYCATEGORY
	 , ISNULL(PC.CATEGORYGROUP__C, '') AS SECTOR_GROUP
INTO #TMP2
FROM #TMP1 P
	LEFT OUTER JOIN VW_PROJECT_CATEGORY PC ON PC.PROJECT__C=P.ID
		AND PC.CATEGORYRANKNUMBER__C = 1

--Obtaining Material Details.
IF OBJECT_ID('TEMPDB..#TMP3') IS NOT NULL
DROP TABLE TEMPDB..#TMP3
SELECT DISTINCT P.*
	 , STUFF((SELECT DISTINCT ','+A.MATERIAL_NAME FROM VW_PROJECT_MATERIAL A WHERE A.PROJECT__C = P.ID FOR XML PATH, TYPE).value('.[1]', 'VARCHAR(MAX)'), 1, 1, '') AS MATERIALS
INTO #TMP3
FROM #TMP2 P

--Obtaining Site Details.
IF OBJECT_ID('TEMPDB..#TMP4') IS NOT NULL
DROP TABLE TEMPDB..#TMP4
;WITH CTE AS (
SELECT DISTINCT PP.*
	 , ISNULL(S1.ADDRESS1__C, '') AS ADDRESSLINE1
	 , ISNULL(S1.ADDRESS2__C, '') AS ADDRESSLINE2
	 , ISNULL(S1.SUBDISTRICT__C, '') AS ADDRESSLINE3
	 , ISNULL(S1.TOWNNAME__C, '') [TOWN]
	 , ISNULL(S1.BOROUGHNAME__C, '') AS BOROUGH
	 , ISNULL(S1.COUNTYNAME__C, '') AS COUNTY
	 , ISNULL(S1.POSTCODE__C, '') AS POSTCODE
	 , ISNULL(T.GOV_REGION, '') AS GOV_REGION
	 , ISNULL(s1.CouncilOSSource__c, '') AS GeoCodeSource
	 , ISNULL(SUBSTRING(S1.COUNCILOSEASTING8__C,2,6), '') AS OS_EASTING
	 , ISNULL(SUBSTRING(S1.COUNCILOSNORTHING8__C,2,6), '') AS OS_NORTHING
	 , ISNULL(CONVERT(VARCHAR, S1.SiteGeoLocation__Latitude__s), '') AS Latitude
	 , ISNULL(CONVERT(VARCHAR, S1.SiteGeoLocation__Longitude__s), '') AS Longitude
	 , ISNULL(CONVERT(VARCHAR, S1.AREA__C), '') AS SITE_AREA
	 , ISNULL(S.PLANNINGREFERENCE__C, '') AS LEADPLANNINGAPPLICATIONNUMBER
	 , ISNULL(LA.LOCAL_AUTHORITY_NAME, '') AS COUNCILNAME
	 , ISNULL(CONVERT(VARCHAR, S.APPLICATIONDATE__C, 106), '') AS LEAD_APPLICATION_SUBMITTED_DATE
	 , S.REFUSEDDATE__C, S.WITHDRAWNDATE__C, S.PERMISSIONDATE__C
	 , ISNULL(S.PLANNINGTYPE__C, '') AS PLANNING_TYPE
	 , ISNULL(CONVERT(VARCHAR, S.APPEALDATE__C, 106), '') AS APPEAL_DATE
	 , CASE WHEN S.PERMISSIONDATE__C IS NULL AND S.REFUSEDDATE__C IS NULL AND S.WITHDRAWNDATE__C  IS NOT NULL THEN 'WITHDRAWN'
			WHEN S.PERMISSIONDATE__C IS NULL AND S.WITHDRAWNDATE__C IS NULL  AND S.REFUSEDDATE__C IS NOT NULL THEN 'REFUSED'
			WHEN S.WITHDRAWNDATE__C IS NULL AND S.REFUSEDDATE__C IS NULL AND  S.PERMISSIONDATE__C IS NOT NULL THEN 'GRANTED' 
	   ELSE '' END [DECISION]
	 , ISNULL(S.FULLAPPLICATIONURL__C, '') AS PLANNING_APPLICATION_URL
	 , ISNULL(LA.APPLICATIONSURL__C, '') AS LA_URL
	 , ISNULL(s.APPLICATIONIDPUBLISH__C, '') APPLICATIONID
	 , ROW_NUMBER() OVER(PARTITION BY PROJECTID ORDER BY PROJECTID, APPLICATIONIDPUBLISH__C DESC) Rw_Id
--INTO #TMP4
FROM #TMP3 PP
	INNER JOIN VW_PROJECT_WITH_SITE_PROPOSAL S ON S.PROJECT__C=PP.ID
	INNER JOIN GR_SITEPROJECTLINK__C SPL ON SPL.PROJECT__C = PP.ID
	INNER JOIN GR_SITE__C S1 ON S1.ID=SPL.SITE__C
	LEFT OUTER JOIN VW_LOCAL_AUTHORITY_COUNTRY LA ON LA.GLOCAL_ID=S.LOCALAUTHORITY__C
	LEFT OUTER JOIN VW_TOWN_COUNTY_REGION T ON T.TOWN_ID=S1.TOWN__C
WHERE S.ACTIVEPROPOSAL__C = 1
	AND LEN(ISNULL(SPL.SITE__C, '')) > 0
	AND CONVERT(DATE, s.APPLICATIONDATE__C) BETWEEN '2008-01-01' AND '2016-02-28'
	AND s.PLANNINGREFERENCE__C <> 'N/A'
	AND s1.CouncilOSSource__c IN ('Auto Location', 'GID -  - outside boundary', 'GID - CP', 'GID - CP - not processable', 'GID - CP - outside boundary', 'GID - LA', 'GID - OS',
								  'GID - OS - not processable', 'GID - OS - outside boundary', 'GID - Unknown')
)
SELECT p.*
INTO #TMP4
FROM CTE p
WHERE Rw_Id = 1

--Aligning the required Fields.
IF OBJECT_ID('TEMPDB..#TMP5') IS NOT NULL
DROP TABLE TEMPDB..#TMP5
SELECT DISTINCT PROJECTID, HEADING, PROJECT_NAME, ADDRESSLINE1, ADDRESSLINE2, ADDRESSLINE3, TOWN, BOROUGH, COUNTY, POSTCODE, GOV_REGION, VALUE, PLANNINGSTAGE, CONTRACTSTAGE, STARTDATE, ENDDATE
	 , CONTRACTPERIOD, DEV_TYPE, PROJECT_SIZE, PROJECTSTATUS, SITE_AREA, FLOORAREA, UNITS, STOREYS, PRIMARYSECTORS, PRIMARYCATEGORY, SECTOR_GROUP, GeoCodeSource, OS_EASTING, OS_NORTHING
	 , ISNULL(MATERIALS, '') MATERIALS, LATESTINFORMATION, SCHEMEDESCRIPTION, LEADPLANNINGAPPLICATIONNUMBER, LEAD_APPLICATION_SUBMITTED_DATE, PLANNING_TYPE, Latitude, Longitude
	 , CASE WHEN [DECISION] = 'REFUSED' THEN ISNULL(CONVERT(VARCHAR, REFUSEDDATE__C, 106), '')
			WHEN [DECISION] = 'WITHDRAWN' THEN ISNULL(CONVERT(VARCHAR, WITHDRAWNDATE__C, 106), '')
			WHEN [DECISION] = 'GRANTED' THEN ISNULL(CONVERT(VARCHAR, PERMISSIONDATE__C, 106), '')
	   ELSE '' END LEAD_APPLICATION_DECISION_DATE, DECISION, COUNCILNAME, FIRSTPUBLISHEDDATE, LASTRESEARCHEDDATE, PLANNING_APPLICATION_URL, LA_URL
INTO #TMP5
FROM #TMP4 P

--Final Select Statement to fetch the required Output.
SELECT DISTINCT PROJECTID, HEADING, PROJECT_NAME, ADDRESSLINE1, ADDRESSLINE2, ADDRESSLINE3, TOWN, BOROUGH, COUNTY, POSTCODE, GOV_REGION, VALUE, PLANNINGSTAGE, CONTRACTSTAGE, STARTDATE, ENDDATE
	 , CONTRACTPERIOD, DEV_TYPE, PROJECT_SIZE, PROJECTSTATUS, SITE_AREA, FLOORAREA, UNITS, STOREYS, GEOCODESOURCE, LATITUDE, LONGITUDE, OS_EASTING, OS_NORTHING, PRIMARYSECTORS, PRIMARYCATEGORY
	 , SECTOR_GROUP, MATERIALS, LATESTINFORMATION, SCHEMEDESCRIPTION, LEADPLANNINGAPPLICATIONNUMBER, LEAD_APPLICATION_SUBMITTED_DATE, LEAD_APPLICATION_DECISION_DATE, DECISION, COUNCILNAME
	 , FIRSTPUBLISHEDDATE, LASTRESEARCHEDDATE
INTO TMP_Geocode
FROM #TMP5
