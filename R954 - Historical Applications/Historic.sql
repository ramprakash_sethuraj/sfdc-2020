SELECT DISTINCT H.HOUSE_EXTN_ID_P, H.CUST_HOUSE_EXTN_ID, H.HEADING, H.TOWN_CD
, T.TOWN_NAME, T.COUNTY_CD, CO.COUNTY_NAME
, C.HOUSE_EXTN_CATEGORY_DESC
, A.PERMISSION_DT
FROM PH_HOUSE_EXTN H
LEFT OUTER JOIN L_TOWN T ON T.TOWN_CD_P = H.TOWN_CD
LEFT OUTER JOIN L_COUNTY CO ON CO.COUNTY_CD_P = T.COUNTY_CD
LEFT OUTER JOIN PH_APPLICATION A ON A.HOUSE_EXTN_ID = H.HOUSE_EXTN_ID_P
LEFT OUTER JOIN PH_HOUSE_EXTN_CATEGORY C ON C.HOUSE_EXTN_ID_P = H.HOUSE_EXTN_ID_P
WHERE CONVERT(DATE, A.PERMISSION_DT) BETWEEN '01-JAN-1999' AND '31-DEC-2014'
AND (CO.COUNTY_NAME IN ('Cambridgeshire', 'Hertfordshire', 'Bedfordshire', 'Northamptonshire') OR T.TOWN_NAME = 'Peterborough')
AND (C.CATEGORY_CD_P = 50 OR C.HOUSE_EXTN_CATEGORY_DESC = 'CONSERVATORY')

