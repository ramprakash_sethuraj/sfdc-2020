USE SFDC_DBAMP

--Obtaining project id.
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP
SELECT DISTINCT p.ID
	, p.PROJECTIDPUBLISH__C
	, p.VALUE__C
	, p.PROJECTSIZENAME
	, co.CONTRACT_CREATEDDATE
	, co.ROLE_EXTERNALID
	, co.ROLE_NAME
	, co.ULTIMATECOMPANY_NAME
	, co.ULTIMATECOMPANYID__C
	, co.ULTIMATECOMPANY__C
	, ISNULL(t.GOV_REGION, '') AS GOV_REGION
INTO #TMP
FROM vw_PROJECT p
INNER JOIN vw_Contract_Office co ON co.PROJECT__C=p.ID
		AND	ISNULL(co.Contract_Deceased, 0) = 0
		AND ISNULL(co.Contract_MPSNoMail, 0) = 0
		AND ISNULL(co.OFFICE_NOLONGERTRADING, 0) = 0
		AND co.OFFICE_PUBLISH = 1
		AND ISNULL(co.OFFICEIDPUBLISHNEW__C, '') <> 1
INNER JOIN GR_SITEPROJECTLINK__C spl ON spl.PROJECT__C = p.id
INNER JOIN GR_SITE__C s1 ON s1.ID=spl.SITE__C
LEFT OUTER JOIN vw_Town_County_Region t ON t.TOWN_ID=s1.TOWN__C
WHERE p.PROJECTSIZENAME IN ( 'Large', 'Mega')
	AND ISNULL(p.recalltype__C,'') <> 'Duplicate'
	AND ISNULL(p.PROJECT_RECORDTYPE, '') <> 'Mover'
	AND p.PUBLISH__C = 1
	AND LEN(ISNULL(spl.SITE__C, '')) > 0
	AND (ISNULL(p.CONTRACTTYPE_NAME,'') NOT IN ('Framework Agreement', 'Measured Term')
		AND (
			(p.PROJECT_DESCRIPTION NOT LIKE '%framework%' AND p.PROJECT_DESCRIPTION NOT LIKE '%measured term%' AND p.PROJECT_DESCRIPTION NOT LIKE '%term maintenance%')    
			AND 
			(p.PROJECT_NAME NOT LIKE '%framework%' AND p.PROJECT_NAME NOT LIKE '%measured term%' AND p.PROJECT_NAME NOT LIKE '%term maintenance%')
			)
		)
	AND CONVERT(DATE,co.CONTRACT_CREATEDDATE) between '2019-07-01' and '2019-09-30'
	AND (co.ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')
		OR co.ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')
		OR co.ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')
		OR co.ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')
		)


--select * from #TMP

----Main contractor 

-- 1st ranking (A whole UK (including Northern Ireland))
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')) a
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 2nd ranking (North East & Yorkshire)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')) a
WHERE  GOV_REGION IN ('N.EAST','YORK.HUMB')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 3rd ranking (North West)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')) a
WHERE  GOV_REGION = 'N.WEST'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 4th ranking (Scotland)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')) a
WHERE  GOV_REGION = 'SCOT'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 5th ranking (East Midlands and East of England (please call this one East of England))
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')) a
WHERE  GOV_REGION IN ('E.MID','EAST')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 6th ranking (London)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')) a
WHERE  GOV_REGION = 'LONDON'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 7th ranking (South East)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')) a
WHERE  GOV_REGION = 'S.EAST'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 8th ranking (South West and Wales)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')) a
WHERE  GOV_REGION IN ('S.WEST','WALES')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 9th ranking (West Midlands)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')) a
WHERE  GOV_REGION = 'W.MID'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 10th ranking (Northern Ireland)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1700', '1701', '1710', '1711', '1715', '1720', '1730', '1735', '1736', '1740', '1750', '1775', '1790')) a
WHERE  GOV_REGION = 'N.IRE'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC



----Civil contractor 

-- 1st ranking (A whole UK (including Northern Ireland))
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')
		) a
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 2nd ranking (North East & Yorkshire)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')) a
WHERE  GOV_REGION IN ('N.EAST','YORK.HUMB')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 3rd ranking (North West)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')) a
WHERE  GOV_REGION = 'N.WEST'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 4th ranking (Scotland)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')) a
WHERE  GOV_REGION = 'SCOT'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 5th ranking (East Midlands and East of England (please call this one East of England))
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')) a
WHERE  GOV_REGION IN ('E.MID','EAST')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 6th ranking (London)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')) a
WHERE  GOV_REGION = 'LONDON'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 7th ranking (South East)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')) a
WHERE  GOV_REGION = 'S.EAST'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 8th ranking (South West and Wales)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')) a
WHERE  GOV_REGION IN ('S.WEST','WALES')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 9th ranking (West Midlands)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')) a
WHERE  GOV_REGION = 'W.MID'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 10th ranking (Northern Ireland)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1770', '1553', '1555', '1502', '1522', '1503', '1577')) a
WHERE  GOV_REGION = 'N.IRE'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC




----Structural contractor 

-- 1st ranking (A whole UK (including Northern Ireland))
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')
		) a
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 2nd ranking (North East & Yorkshire)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')) a
WHERE  GOV_REGION IN ('N.EAST','YORK.HUMB')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 3rd ranking (North West)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')) a
WHERE  GOV_REGION = 'N.WEST'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 4th ranking (Scotland)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')) a
WHERE  GOV_REGION = 'SCOT'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 5th ranking (East Midlands and East of England (please call this one East of England))
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')) a
WHERE  GOV_REGION IN ('E.MID','EAST')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 6th ranking (London)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')) a
WHERE  GOV_REGION = 'LONDON'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 7th ranking (South East)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')) a
WHERE  GOV_REGION = 'S.EAST'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 8th ranking (South West and Wales)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')) a
WHERE  GOV_REGION IN ('S.WEST','WALES')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 9th ranking (West Midlands)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')) a
WHERE  GOV_REGION = 'W.MID'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 10th ranking (Northern Ireland)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1514', '1501', '1521', '1541', '1535', '1519', '1582', '1583', '1589', '1590', '1585', '1525', '1526')) a
WHERE  GOV_REGION = 'N.IRE'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC




----Internal contractor 

-- 1st ranking (A whole UK (including Northern Ireland))
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')
		) a
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 2nd ranking (North East & Yorkshire)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')) a
WHERE  GOV_REGION IN ('N.EAST','YORK.HUMB')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 3rd ranking (North West)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')) a
WHERE  GOV_REGION = 'N.WEST'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 4th ranking (Scotland)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')) a
WHERE  GOV_REGION = 'SCOT'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 5th ranking (East Midlands and East of England (please call this one East of England))
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')) a
WHERE  GOV_REGION IN ('E.MID','EAST')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 6th ranking (London)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')) a
WHERE  GOV_REGION = 'LONDON'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 7th ranking (South East)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')) a
WHERE  GOV_REGION = 'S.EAST'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 8th ranking (South West and Wales)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')) a
WHERE  GOV_REGION IN ('S.WEST','WALES')
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 9th ranking (West Midlands)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')) a
WHERE  GOV_REGION = 'W.MID'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC

-- 10th ranking (Northern Ireland)
select ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME,COUNT(DISTINCT PROJECTIDPUBLISH__C) AS [No of Projects], sum(VALUE__C) AS [Value of Projects]
from (SELECT DISTINCT ULTIMATECOMPANYID__C , ULTIMATECOMPANY_NAME, PROJECTIDPUBLISH__C, VALUE__C , GOV_REGION
		from #TMP
		WHERE ROLE_EXTERNALID IN ('1549', '1528', '1548', '1775', '1507', '1561', '1530', '1520', '1563', '1564', '1531', '1537', '1538', '1544', '1558')) a
WHERE  GOV_REGION = 'N.IRE'
group by ULTIMATECOMPANYID__C, ULTIMATECOMPANY_NAME
ORDER BY [Value of Projects] DESC
