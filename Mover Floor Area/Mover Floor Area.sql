USE SFDC_DBAMP

-- 51798
IF OBJECT_ID('TEMPDB..#TMP') IS NOT NULL
DROP TABLE TEMPDB..#TMP
SELECT DISTINCT p.id as [Salesforce ID]
	, p.MoverIdPublishNew__c AS [Mover ID]
	, ISNULL(convert(VARCHAR, FloorAreaFeet__c), '') AS [Floor Area]
	, ISNULL(convert(VARCHAR, MoverMovingStaffNo__c), '') AS [Moving staff number]
	, ISNULL(convert(VARCHAR, MoverTotalStaffNo__c), '') AS [Total staff number]

INTO #TMP

FROM gr_project__C p 
	INNER JOIN RECORDTYPE rt ON p.RecordTypeId = rt.id
WHERE rt.NAME = 'Mover'
	AND ISNULL(P.RECALLTYPE__C, '') <> 'DUPLICATE'
	AND p.publish__c = 1
	AND FloorArea__c IS NULL

--51798

-- To Region of the Mover
IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL
DROP TABLE TEMPDB..#TMP1
SELECT DISTINCT p.*, ISNULL(T.GOV_REGION, '')  AS [Move to region]

INTO #TMP1

FROM #TMP p
	INNER JOIN GR_SITEPROJECTLINK__C spl ON spl.PROJECT__C=P.[Salesforce ID] 
	INNER JOIN gr_site__C S ON s.id = spl.Site__c
	LEFT OUTER JOIN VW_TOWN_COUNTY_REGION T ON T.TOWN_ID=s.TOWN__C
WHERE LEN(ISNULL(spl.SITE__C, '')) > 0




--FINAL SELECT 	
SELECT [Mover ID], [Salesforce ID], [Floor Area], [Moving staff number], [Total staff number], [Move to region]
FROM #TMP1












